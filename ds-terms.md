
# Interfaces

- List  - Lista
- Stack - Pila
- Queue - Cola
- Deque (Double-ended queue) - Cola de terminación doble
- USet (Unordered Set) - Conjunto Desordenado
- SSet (Sorted Set) - Conjunto Ordenado

# Estructuras de datos

- ArrayStack - 
- ArrayQueue - Implementa la interfaz #Queue# (FIFO) usando un array.
- ArrayDeque - 
- DualArrayDeque - Implementa la interfaz #List# usando dos #ArrayStack#s.
- SLList (Singly-Linked List) - Lista Singularmente Enlazada
- DLList (Doubly-Linked List) - Lista Doblemente Enlazada
- SEList (Space-Eficiente List) - Lista con Eficiencia Espacial
