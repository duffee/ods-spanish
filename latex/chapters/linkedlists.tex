\chapter{Listas Enlazadas}
\chaplabel{linkedlists}

\index{lista!enlazada}%
En este capítulo, continuamos con en el estudio de las implementaciones de la
interfaz #List#, pero usando esta vez estructuras de datos basadas en punteros
y no en arrays. Las estructuras en esta capítulo están compuestas por nodos que
contienen los artículos de la lista. Usando referencias (punteros), los nodos
se enlazan para formar una secuencia. Primero estudiaremos listas simplemente
enlazadas, las cuales pueden implementan las operaciones de las interfaces #Stack# y
#Queue# (FIFO) en tiempo constante por operación y después nos moveremos a las
listas doblemente enlazadas, las cuales pueden implementar las operaciones de
una #Deque# en tiempo constante.

Las listas enlazadas tienen ventajas y desventajas cuando las comparamos con las
implementaciones basadas en array de la interfaz #List#. La principal desventaja es 
que perdemos la habilidad de acceder cualquier elemento usando #get(i)# o #set(i, x)#
en tiempo constante. En lugar de eso, tenemos que recorrer la lista, un elemento a la
vez, hasta que lleguemos al elemento #i#$\ulo$. La ventaja primaria es que son 
más dinámicas: Dada una referencia a cualquier nodo #u# de la lista, podemos
remover #u# o insertar un nodo adyacente a #u# en tiempo constante. Esto es
verdadero sin importar donde #u# se encuentre en la lista.

\section{#SLList#: Una Lista Singularmente Enlazada}
\seclabel{sllist}

\index{estructuras de datos!SLList@#SLList#}%
\index{lista!singularmente enlazada}%
Una #SLList# (lista singularmente enlazada) es una secuencia de #Nodes#. Cada 
nodo #u# almacena un valor #u.x# y una referencia #u.next# al nodo siguiente en
la secuencia. Para el último nodo #w# en la secuencia, $#w.next# = #null#$

% TODO: Remove constructors from SLList.Node
\javaimport{ods/SLList.Node}
\cppimport{ods/SLList.Node}

Por razones de eficiencia, una #SLList# usa las variables #head# y #tail# para 
mantener récord de los dos nodos (primero y último) al extremo de la secuencia,
como un entero #n# para mantener récord de la longitud de la secuencia:
\codeimport{ods/SLList.head.tail.n}
Una secuencia de las operaciones de una #Stack# y una #Queue# sobre sobre una #SLList#
se ilustra en \figref{sllist}.

\begin{figure}
  \begin{center}
    \includegraphics[width=\ScaleIfNeeded]{figs/sllist}
  \end{center}
  \caption[Una secuencia de las operaciones de una #Stack# y una #Queue# sobre
    una #SLList#]{Una secuencia de las operaciones de #Queue# (#add(x)# y
    #remove()#) y #Stack# (#push(x)# y #pop()#) sobre una #SLList#.}
  \figlabel{sllist}
\end{figure}

Una #SLList# puede implementar eficientemente las operaciones #push()# y #pop()#
de una #Stack# al añadir y remover elementos en la cabezera de la secuencia.
La operación #push()# simplemente crea un nodo nuevo #u# con valor #x#,
asigna #u.next# a la antigua cabezera de la lista y hace a #u# la nueva cabecera
de la lista. Finalmente, la operación incrementa #n# dado que el tamaño de la
#SLList# ha aumentado por uno:

\codeimport{ods/SLList.push(x)}

La operación #pop()#, después de verificar que la #SLList# no está vacía,
remueve la cabezera al asignar $#head=head.next#$ y disminuir #n#.
Un caso especial ocurre cuando el último elemento se remueve, y en tal caso
#null# es asignado a #tail#:

\codeimport{ods/SLList.pop()}

Claramente, ambas operaciones #push(x)# y #pop()# se ejecutan en tiempo $O(1)$.

\subsection{Operaciones de una Cola}

Una #SLList# puede también implementar las operaciones #add(x)# y #remove()# 
de una cola FIFO en tiempo constante. Las remociones se hacen en la cabecera de
la lista, y son idénticas a la operación #pop()#:

\codeimport{ods/SLList.remove()}

Adiciones, por el otro lado, se hacen en la cola de la lista. En la mayoría de 
los casos, esto se hace con $#tail.next#=#u#$, donde #u# es el nodo
recientemente creado que contiene #x#. Sin embargo, un caso especial ocurre
cuando $#n#=0$, y en tal caso $#tail#=#head#=#null#$.  En este caso, 
#u# se asigna a las variables #tail# y #head#.

\codeimport{ods/SLList.add(x)}

Claramente, ambas operaciones #add(x)# y #remove()# toman tiempo constante.

\subsection{Summary}

El teorema siguiente sintetiza el rendimiento de una #SLList#:

\begin{thm}\thmlabel{sllist}
    Una #SLList# implementa las interfaces #Stack# y #Queue# (FIFO). 
    Las operaciones #push(x)#, #pop()#, #add(x)# y #remove()# se ejecutan
    en tiempo $O(1)$ por operación.
\end{thm}

Una #SLList# casi implementa el conjunto de las operaciones de una #Deque#.
La única operación que falta es la remoción desde la cola de la lista.
Remover desde la cola de una #SLList# es difícil porque requiere actualizar
el valodr de #tail# para que apunte al nodo #w# que precede #tail# en la
#SLList#; este es el nodo #w# tal que $#w.next#=#tail#$.  Desafortunadamente,
la única manera de conseguir a #w# es al traversar la #SLList# comenzando por
#head# y tomando $#n#-2$ pasos.

\section{#DLList#: Una Lista Doblemente Enlazada}
\seclabel{dllist}

\index{estructuras de datos!DLList@#DLList#}%
\index{lista!doblemente enlazada}%
Una #DLList# (lista doblemente enlazada) es muy similar a una #SLList# excepto
que cada nodo #u# en una #DLList# tiene referencias al nodo #u.next#
que le sigue y al nodo #u.prev# que le antecede.

\javaimport{ods/DLList.Node}
\cppimport{ods/DLList.Node}

Al implementar una #SLList#, vimos que siempre hay varios casos especiales de
los cuales tenemos que preocuparnos. Por ejemplo, remover el último elemento 
de una #SLList# o agregar un elemento a una #SLList# vacía requiere una garantía
de que #head# y #tail# están actualizados correctamente. En una #DLList#,
el número de estos casos especiales crece considerablemente. Quizás la manera
más concisa de encargarse de todos estos casos especiales en una #DLList#
es introducir un nodo #dummy# (\textit{ficticio}).

\index{nodo!ficticio (dummy)}%
Este nodo no contiene ningún dato, pero actúa como un marcador para que así no
hayan nodos especiales; cada nodo tiene un #next# y un #prev#, con #dummy# 
actuando como el nodo que sigue el último nodo en la lista y que precede 
el primer nodo en la lista. En esta manera, los nodos de la lista están
(doblemente) enlazados en un ciclo, como se ilustra en la \figref{dllist}.

\begin{figure}
  \begin{center}
    \includegraphics[width=\ScaleIfNeeded]{figs/dllist2}
  \end{center}
  \caption[Una DLList]{Una #DLList# que contiene los elementos a,b,c,d,e.}
  \figlabel{dllist}
\end{figure}


%TODO: Remove constructors from class Node

\codeimport{ods/DLList.n.dummy.DLList()}

Encontrar el nodo con un índice particular en una #DLList# es fácil; podemos
comenzar en la cabezera de la lista (#dummy.next#) y seguir hacia adelante,
o comenzar en la cola de lista (#dummy.prev#) y retroceder.
Esto nos permite alcanzar el nodo #i#$\ulo$ en tiempo
$O(1+\min\{#i#,#n#-#i#\})$:

\codeimport{ods/DLList.getNode(i)}

Las operaciones #get(i)# y #set(i,x)# son ahora igualmente fáciles.
Primero encontramos el nodo #i#$\ulo$ y después obtenemos o asignamos su valor 
#x#:

\codeimport{ods/DLList.get(i).set(i,x)}

El tiempo de ejecución de estas operaciones es dominaod por el tiempo que toma
en encontrar el nodo #i#$\ulo$, y es por lo tanto $O(1+\min\{#i#,#n#-#i#\})$.

\subsection{Agregar y Remover}

Si tenemos una referencia a un nodo #w# en una #DLList# y queremos insertar un
nodo #u# antes que #w#, entonces esto es solamente un asunto de asignar 
$#u.next#=#w#$, $#u.prev#=#w.prev#$, y después adjustar #u.prev.next# y 
#u.next.prev#.  (Ver la \figref{dllist-addbefore}.) Gracias al nodo falso, 
no tenemos que preocuparnos sobre la existencia de #w.prev# o #w.next#.

\codeimport{ods/DLList.addBefore(w,x)}

\begin{figure}
   \begin{center}
      \includegraphics[scale=0.90909]{figs/dllist-addbefore}
   \end{center}
   \caption[Agregando a una DLList]{Agregando el nodo #u# antes que el nodo #w#
      en una #DLList#.}
   \figlabel{dllist-addbefore}
\end{figure}

Ahora, la operación #add(i,x)# de la lista es trivial. Es básicamente un asunto
de encontrar el nodo #i#$\ulo$ en la #DLList# e insertar un nuevo nodo #u# que 
contiene a #x# antes que el mismo.

\codeimport{ods/DLList.add(i,x)}

La única parte que no es constante del tiempo de ejecución de #add(i,x)# 
es el tiempo que toma en encontrar el nodo #i#$\ulo$ (usando #getNode(i)#). Así que,
#add(i,x)# se ejecuta en tiempo $O(1+\min\{#i#, #n#-#i#\})$.

Remover un nodo #w# de una #DLList# es fácil. Solamente necesitamos ajustar los
punteros a #w.next# y a #w.prev# para que salten sobre #w#.  Nuevamente, el uso 
del nodo ficticio elimina la necesidad de considerar cualesquiera casos
especiales:

\codeimport{ods/DLList.remove(w)}

Ahora la operación #remove(i)# es trivial. Encontramos el nodo con índice #i# y
lo removemos:

\codeimport{ods/DLList.remove(i)}

Otra vez, la única parte costoda de esta operación es encontrar el nodo
#i#$\ulo$ usando a #getNode(i)#, así que #remove(i)# se ejecuta en tiempo 
$O(1+\min\{#i#, #n#-#i#\})$.

\subsection{Resumen}

El teorema siguiente sintetiza el rendimiento de una #DLList#:

\begin{thm}\thmlabel{dllist}
  Una #DLList# implementa la interfaz #List#.  En esta implementación,
  las operaciones #get(i)#, #set(i,x)#, #add(i,x)# y #remove(i)# se ejecutan
  en tiempo $O(1+\min\{#i#,#n#-#i#\})$ por cada operación.
\end{thm}

Es importante mencionar que, si ignoramos el costo de la operación #getNode(i)#,
entonces todas las operaciones sobre una #DLList# se ejecutan en tiempo
constante. Así que, la única parte costosa de las operaciones sobre una #DLList#
es encontrar el nodo relevante.  Una vez que tenemos el nodo relevante, agregar,
remover o acceder a los datos en ese nodo toma solamente tiempo constante.

Este es un contraste pronunciado con respecto a las implementaciones basadas en
array de #List# del \chapref{arrays}; en aquellas  those implementaciones,
el artículo del array relevante puede encontrarse en tiempo constante. Sin
embargo, la addición o la remoción requiere el desplazamiento de los elementos 
en el array y, en general, toma tiempo que no es constante.

Por esta razón, las estructuras de lista enlazada son adecuadas para
aplicaciones donde las referencias a los nodos de la lista pueden obtenerse 
a través de medios externos. 
\javaonly{An example of this is the #LinkedHashSet# data structure found in the
Java Collections Framework, in which a set of items is stored in a
doubly-linked list and the nodes of the doubly-linked list are stored
in a hash table (discussed in \chapref{hashing}).  When elements
are removed from a #LinkedHashSet#, the hash table is used to find the
relevant list node in constant time and then the list node is deleted
(also in constant time).}
\cpponly{For example, pointers to the nodes of a linked list could be
stored in a #USet#.  Then, to remove an item #x# from the linked list,
the node that contains #x# can be found quickly using the #Uset# and
the node can be removed from the list in constant time.}

\section{#SEList#: Una Lista Enlazada con Eficiencia Espacial}
\seclabel{selist}

\index{estructuras de datos!SEList@#SEList#}%
\index{lista!enlazada con eficiencia espacial}%
Una de las desventajas de las listas enlazadas (además del tiempo que toma en 
acceder los elementos que se encuentran en lo profundo de la lista) es su usu
del espacio. Cada nodo en una #DLList# requiere dos referencias adicionales
a los nodos siguientes y previos en la lista. Dos de las casillas en un 
#Node# están dedicada a mantener la lista, y solamente una de las casillas
es para almacenar los datos.

Una #SEList# (lista con eficiencia espacial) reduce este espacio malgastado
usando una idea simple:  En vez de almacenar los elementos individuales en una 
#DLList#, almacenamos un bloque (array) que contiene varios artículos. 
Más precisamente, una #SEList# es parametrizada por un \emph{tamaño de bloque}
#b#. Cada nodo individual en una #SEList# almacena un bloque que 
que puede contener #b+1# elementos.

Por razones que serán aparentes más tarde, será útil si podemos realizar
operaciones de una #Deque# sobre cada bloque. La estructura de datos que 
elegimos para esto es una #BDeque# (deque limitada),
\index{estructuras de datos!BDeque@#BDeque#}%
\index{deque!limitada}%
derivada de la estructura #ArrayDeque# descrita en la \secref{arraydeque}.
La #BDeque# difiere de la #ArrayDeque# en un detalle pequeño:
Cuando una #BDeque# nueva es creada, el tamaño del array de respaldo #a# se 
fija en #b+1# y nunca crece o se contrae.
La propiedad importante de una #BDeque# es que permite la adición 
o remoción de los elementos al frente o al final en tiempo constant. 
Esto será útil a medida que los elementos son desplazados de un bloque a otro.

\javaimport{ods/SEList.BDeque} 
\cppimport{ods/SEList.BDeque} 

\notpcode{
Una #SEList# es entonces una lista doblemente enlazada de bloques:
} % notpcode
\javaimport{ods/SEList.Node}
\cppimport{ods/SEList.Node}
\javaimport{ods/SEList.n.dummy}
\cppimport{ods/SEList.n.dummy}

\pcodeonly{
Una #SEList# es entonces una lista doblemente enlazada de bloques. En
adición a los punteros #next# y #prev#, cada nodo #u# en una #SEList# contiene 
una #BDeque#, #u.d#.}

\subsection{Requerimientos Espaciales}

Una #SEList# coloca restricciones muy estrictas en el número de elementos
en un bloque: A menos que un bloque sea el último
bloque, entonces ese bloque en particular contiene por lo menos $#b#-1$
elementos y como máximo $#b#+1$. Esto significa que, si una #SEList# contiene 
#n# elementos, entonces tiene como máximo
\[
    #n#/(#b#-1) + 1 = O(#n#/#b#)
\]
bloques.  La estructura #BDeque# para cada bloque contiene un array de longitud $#b#+1$
pero, por cada bloque excluyendo el último, un número constante de espacio 
es malgastado en este array como máximo. La memoria restante usada por cada bloque es
igualmente constante. Esto significa que el espacio gastado en una #SEList# es solamente
$O(#b#+#n#/#b#)$. Al elegir un valor de #b# dentro de un factor constante de 
$\sqrt{#n#}$, podemos hacer que el espacio general utilizado por una #SEList#
alcance el límite inferior $\sqrt{#n#}$ dado en \secref{rootishspaceusage}.

\subsection{Encontrando Elementos}

El primer reto al cual nos enfrentamos con una #SEList# es encontrar el artículo
de la lista con un índice #i# dado. Nota que la posición de un elemento consiste
de dos partes:
\begin{enumerate}
  \item El node #u# que contiene el bloque que a su vez contiene el elemento 
      con índice #i#; y
  \item el índice #j# del elemento dentro del bloque.
\end{enumerate}

\javaimport{ods/SEList.Location}
\cppimport{ods/SEList.Location}

Para encontrar el bloque que contiene un elemento particular, procedemmos 
de la misma manera en la cual lo hacemos en una #DLList#. Comenzamos al frente de la
lista y la traversamos hacia adelante, o en la cola de la lista y la recorremos 
hacia atrás hasta que encontremos el nodo que queremos. La única diferencia es que, 
cada vez que nos movemos desde un nodo al siguiente, saltamos sobre un bloque 
completo de elementos.

\pcodeimport{ods/SEList.get_location(i)}
\javaimport{ods/SEList.getLocation(i)}
\cppimport{ods/SEList.getLocation(i,ell)}

Ten presente que, con la excepción de como máximo un bloque, cada bloque
contiene por lo menos $#b#-1$ elementos, así que cada paso en nuestra búsqueda
nos acerca $#b#-1$ elementos al elemento que busquemos.  Si estamos haciendo la
búsuqeda hacia adelante, esto significa que hemos alcanzamos el nodo que
queremos después de $O(1+#i#/#b#)$ pasos.  Si hacemos la búsqueda hacia atrás,
entonces alcanzamos el nodo que queremos después $O(1+(#n#-#i#)/#b#)$ pasos.
El algoritmos toma la más pequeña de estas dos cantidaes dependiendo del valor
de #i#, así que el tiempo tomado para localizar el artículo con índice #i# 
es $O(1+\min\{#i#,#n#-#i#\}/#b#)$.

Una vez que sabemos cómo localizar el artículo con índice #i#, las operaciones
#get(i)# y #set(i,x)# se traducen en obtener o asignar a un índice particular
en el bloque correcto:

\codeimport{ods/SEList.get(i).set(i,x)}

Los tiempos de ejecución de estas operaciones están dominados por el tiempo
que se toma en localizar el artículo, así que se ejecutan en tiempo
$O(1+\min\{#i#,#n#-#i#\}/#b#)$.

\subsection{Agregando un Elemento}

Agregar elementos a una #SEList# es un poco más complicado.  Antes de considerar
el caso general, consideremos la operación #add(x)# más fácil, en la cual 
#x# se agrega al final de la lista. Si el últim bloque está lleno
(\textit{ó} no existe porque no hay bloques aún), entonces primero asignamos 
un bloque nuevo y lo agregamo a la lista de bloques. Ahora que estamos seguros
que el último  bloque existe y no está lleno, agreamos #x# al último 
bloque.

\cppimport{ods/SEList.add(x)}
\javaimport{ods/SEList.add(x)}
\pcodeimport{ods/SEList.append(x)}

Las cosas se complican cuando agregamos al interior de la lista usando 
#add(i,x)#.  Primero localizamos #i# para obtener el nodo #u# cuyo bloque
contiene el artículo $#i#\ulo$ de la lista. El problema es que deseamos 
insertar #x# en el bloque de #u#, pero tenemos que estar preparados para el caso
donde el bloque de #x# ya contiene $#b#+1$ elementos, así que está lleno
y por lo tanto no hay espacio para #x#.

Denotamos a #u#, #u.next#, #u.next.next#, y así sucesivamente como
$#u#_0,#u#_1,#u#_2,\ldots$. Exploramos a $#u#_0,#u#_1,#u#_2,\ldots$ buscando un
nodo que pueda proveer espacio para #x#. Tres casos pueden ocurrir 
durante nuestra exploración por la búsqueda de espacio 
(ver la \figref{selist-add}):

\begin{figure}
  \noindent
  \begin{center}
    \begin{tabular}{@{}l@{}}
      \includegraphics[width=\ScaleIfNeeded]{figs/selist-add-a}\\[4ex]
      \includegraphics[width=\ScaleIfNeeded]{figs/selist-add-b}\\[4ex]
      \includegraphics[width=\ScaleIfNeeded]{figs/selist-add-c}\\
    \end{tabular}
  \end{center}
  \caption[Método add de una SEList]{Los tres casos que ocurren durante la adición de una
    rtículo #x# en el interior de una #SEList#. 
    (Esta #SEList# tiene un bloque de tamaño $#b#=3$.)}
  \figlabel{selist-add}
\end{figure}


\begin{enumerate}
\item Rápidamente (en $r+1\le #b#$ pasos) encontamos un nodo $#u#_r$ cuyo bloque
    no está lleno. En este caso, podemos realizar $r$ desplazamientos de un
    elemento desde un bloque a siguiente, para que así el espacio libre en
    $#u#_r$ se convierta en espacio libre $#u#_0$. Podemos entonces insertar a
    #x# en el bloque de $#u#_0$

\item Rápidamente (en $r+1\le #b#$ pasos) alcazamos el final de la lista. En
    este caso, agregamos un nuevo bloque vacío al final de la lista de bloques y
    procedemos como en el primer caso.

\item Después de #b# pasos no encontramos ningún bloque que no esté lleno.
En este caso, $#u#_0,\ldots,#u#_{#b#-1}$ es una secuencia de #b# bloques
y cada uno contiene $#b#+1$ elementos.  Insertamos un nuevo bloque $#u#_{#b#}$
al final de esta secuencia y \emph{esparcimos} los $#b#(#b#+1)$ elementos
originales para que de esta manera, cada bloque de $#u#_0,\ldots,#u#_{#b#}$ 
contenga exactamente #b# elementos. Ahora el bloque de $#u#_0$ contiene
solamente #b# elementos y por lo tanto posee espacio para insertar a #x#.
\end{enumerate}

\codeimport{ods/SEList.add(i,x)}

El tiempo de ejecución de la operación #add(i,x)# depende en cual de los tres
casos ocurre. Casos~1 y 2 involucran la examinación y el desplazamiento de 
de elementos a través de por lo menos #b# bloques y toma tiempo $O(#b#)$. 
Caso~3 involucra la llamada del método #spread(u)#, el cual mueve $#b#(#b#+1)$
elementos y toma tiempo $O(#b#^2)$. Si ignoramos el costo del Caso~3 (el cual
consideraremos más tarde con amortización) esto significa que el tiempo de
ejecución total para localizar a #i# y realizar la inserción de #x# es 
$O(#b#+\min\{#i#,#n#-#i#\}/#b#)$.

\subsection{Removiendo un Elemento}

Remover un elemento de una #SEList# es similar a agregar un elemento.
Primero localizamos el nodo #u# que contiene el elemento con índice #i#.
Ahora, debemos estar preparados para el caso donde no podemos remover un
elemento de #u# sin causar que el bloque de #u# se vuelva más pequeño que 
$#b#-1$.

Otra vez, dejemos $#u#_0,#u#_1,#u#_2,\ldots$ denote a #u#, #u.next#, #u.next.next#,
y así sucesivamente. Examinamos a $#u#_0,#u#_1,#u#_2,\ldots$ para buscar por un
nodo desde el cual podemos tomar prestado un elemento para hacer el tamaño del
bloque de $#u#_0$ por lo menos $#b#-1$.  Existen tres casos a considerar
(ver \figref{selist-remove}):

\begin{figure}
  \noindent
  \begin{center}
    \begin{tabular}{l}
      \includegraphics[scale=0.90909]{figs/selist-remove-a}\\[4ex]
      \includegraphics[scale=0.90909]{figs/selist-remove-b}\\[4ex]
      \includegraphics[scale=0.90909]{figs/selist-remove-c}\\
    \end{tabular}
  \end{center}
  \caption[Métod remove de una SEList]{Los tres casos que ocurren durante la remoción de un
    artículo #x# en el interior de una #SEList#.  (Esta #SEList# tiene bloques
    de tamaño $#b#=3$.)}
  \figlabel{selist-remove}
\end{figure}


\begin{enumerate}
\item Rápidamente (en $r+1\le #b#$ pasos) encontramos un nodo cuyo bloque
contiene más de $#b#-1$ elementos. En este caso, realizamos $r$ desplazamientos 
de un elemento de un bloque al bloque previo, de tal manera que el elemento
extra en $#u#_r$ se convierta en un elemento extra en $#u#_0$. Después podemos
remover el elemento apropiado del bloque de $#u#_0$.

\item Rápidamente (en $r+1\le #b#$ pasos) llegamos al final de la lista de
bloques. En este caso, $#u#_r$ es el último bloque, y no hay necesidad de que el
bloque de $#u#_r$ contenga por lo menos $#b#-1$ elementos.  Por lo tanto, 
procedemos como lo hicimos anteriormente, tomando prestado un elemento de 
$#u#_r$ para creare un elemento extra en $#u#_0$.  Si esto hace que el bloque de 
$#u#_r$ se vacíe, entonces lo removemos.

\item Después de #b# pasos, no encontramos ningún bloque que contiene más de 
$#b#-1$ elementos.  En este caso, $#u#_0,\ldots,#u#_{#b#-1}$ es una secuencia 
de #b# bloques y cada uno contiene $#b#-1$ elementos.  Recolectamos
(\emph{gather}) estos $#b#(#b#-1)$ elementos en $#u#_0,\ldots,#u#_{#b#-2}$ de
tal manera que estos $#b#-1$ bloques contengan exactamente #b# elementos y
removemos a $#u#_{#b#-1}$, que ahora está vacío. Ahora el bloque de $#u#_0$
contiene #b# elementos y podemos entonces remover el elemento apropiado del
mismo. 
\end{enumerate}

\codeimport{ods/SEList.remove(i)}

Similar a la operación #add(i,x)#, el tiempo de ejecución de la operación 
#remove(i)# es $O(#b#+\min\{#i#,#n#-#i#\}/#b#)$ si ignoramos el costo del método
#gather(u)# que ocurre en el Caso~3.

\subsection{Análisis Amortizado del Esparcimiento y la Recolección}

Ahora, consideramos el costo de los métodos #spread(u)# y #gather(u)# que pueden 
ser ejecutados por los métodos #add(i,x)# y #remove(i)#. En aras de la
exhaustividad, aquí lo presentamos:

\codeimport{ods/SEList.spread(u)}
\codeimport{ods/SEList.gather(u)}

El tiempo de ejecución de cada uno de los métodos está dominado por dos bucles
anidados. Ambos bucles, interno y externo, se ejecutan como máximo 
$#b#+1$ veces, así que el total de tiempo de ejecución de cada uno de los
métodos es $O((#b#+1)^2)=O(#b#^2)$. Sin embargo, el lema siguiente demuestra que
estos métodos se ejecutan como máximo una de cada #b# llamadas a #add(i,x)#
o #remove(i)#.

\begin{lem}\lemlabel{selist-amortized}
  Si una #SEList# vacía es creada y cualquier secuencia de $m\ge 1$ llamadas
  a #add(i,x)# y #remove(i)# es realizada, entonces el total de tiempo 
  utilizado durante todas las llamadas a #spread()# y #gather()# es $O(#b#m)$.
\end{lem}

\begin{proof}
  Usaremos el método potencial del análisis amortizado.
  \index{método!potencial}%
  Decimos que un nodo #u# es \emph{frágil} si el bloque de #u#
  no contiene #b# elementos (de tal manera que #u# es el último nodo 
  \emph{ó} contiene $#b#-1$ elementos). Cualquier nodo cuyo bloque contiene 
  #b# elementos es \emph{robusto}. Se define como \emph{potencial} de una 
  #SEList# al número de nodos frágiles que contiene. Consideraremos solamente
  la operación #add(i,x)# y su relación al número de llamadas a #spread(u)#.
  El análisis de #remove(i)# y #gather(u)# es idéntico.

  Notice that, if Case~1 occurs during the #add(i,x)# method, then
  only one node, $#u#_r$ has the size of its block changed. Therefore,
  at most one node, namely $#u#_r$, goes from being rugged to being
  fragile.  If Case~2 occurs, then a new node is created, and this node
  is fragile, but no other node changes size, so the number of fragile
  nodes increases by one.  Thus, in either Case~1 or Case~2 the potential
  of the SEList increases by at most one.

  Finally, if Case~3 occurs, it is because $#u#_0,\ldots,#u#_{#b#-1}$
  are all fragile nodes.  Then $#spread(#u_0#)#$ is called and these #b#
  fragile nodes are replaced with $#b#+1$ rugged nodes.  Finally, #x#
  is added to $#u#_0$'s block, making $#u#_0$ fragile.  In total the
  potential decreases by $#b#-1$.

  En resumen, el potencial empieza con 0 (no hay nodos en la lista).
  Each time Case~1 or Case~2 occurs, the potential increases by at
  most 1.  Each time Case~3 occurs, the potential decreases by $#b#-1$.
  The potential (which counts the number of fragile nodes) is never
  less than 0.  We conclude that, for every occurrence of Case~3, there
  are at least $#b#-1$ occurrences of Case~1 or Case~2.  Thus, for every
  call to #spread(u)# there are at least #b# calls to #add(i,x)#.  This
  completes the proof.
\end{proof}

\subsection{Resumen}

El teorema siguiente sintetiza el rendimiento de la estructura de datos
#SEList#:

\begin{thm}\thmlabel{selist}
  Una #SEList# implementa la interfaz #List#. Ignorando el costo de las llamadas
  a #spread(u)# y #gather(u)#, una #SEList# con bloques de tamaño #b# soporta
  las operaciones
  \begin{itemize}
    \item #get(i)# y #set(i,x)# en tiempo $O(1+\min\{#i#,#n#-#i#\}/#b#)$ por
    operacion;
    \item #add(i,x)# y #remove(i)# en tiempo $O(#b#+\min\{#i#,#n#-#i#\}/#b#)$
        por operación.
  \end{itemize}
  Además, si comenzamos con una #SEList# vacía, cualquier secuencia de $m$
  operaciones #add(i,x)# y #remove(i)# resulta en un total de tiempo $O(#b#m)$
  utilizado durante todas las llamadas a los métodos #spread(u)# y #gather(u)#.

    El espacio (medido en palabras)\footnote{Recuerda \secref{model} para una
    discusión de cómo se mide la memoria.} usado por una 
    #SEList# que almacena #n# elementos es $#n# +O(#b# + #n#/#b#)$.
\end{thm}

La #SEList# es una solución intermedia entre una #ArrayList# y una #DLList#
donde la mezcla relativa de dos estructuras depende en los bloques de tamaño
#b#. Al extremo $#b#=2$, cada nodo de una #SEList# almacena como máximo tres
valores, lo cual no es tan diferente a #DLList#. Al otro extremo, 
$#b#>#n#$, todos los elemento son almacenados en un único array, 
al igual que en una #ArrayList#.  En el medio de estos dos extremos
se encuentran una solución intermedia entre el tiempo que se toma en agregar o
remover un artículo de la lista y el tiempo que se toma en localizar 
un elemento particular de la lista.

\section{Discusión y Ejercicios}

Las listas simplemente y doblemente enlazadas son técnicas establecidas, las
cuales han sido utilizadas en programas por más de 40 años. Por ejemplo, 
Knuth \cite[Sections~2.2.3--2.2.5]{k97v1} escribe sobre las mismas.  Incluso la
estructura de datos #SEList# parece ser un ejercicio de estructura de datos muy
conocido. La #SEList# es alguna veces referida como \emph{unrolled linked list}
\cite{sra94}.
\index{lista!enlazada \emph{unrolled}|seealso{#SEList#}}%

Otra manera de ahorrar espacio en una lista doblemente enlazada es al usar las
llamadas listas XOR.
\index{lista!XOR}%
En una lista XOR, cada nodo, #u#, contiene solamente un puntero, llamado 
#u.nextprev#, que almacena el OR exclusivo bitwise de #u.prev#
y #u.next#.  La lista en si misma necesita almacenar dos punteros, un puntero 
al nodo #dummy# y otro al #dummy.next# (el primer nodoe, o #dummy# si la lista 
está vacía). Esta técnica usa el hecho de que, si tenemos punteros a #u#
y #u.prev#, entonces podemos extraer a #u.next# usando la fórmula
\[
   #u.next# = #u.prev# \verb+^+ #u.nextprev# \enspace .
\]
( \verb+^+ calcula el OR exclusivo bitwise de sus dos argumentos.)
Esta técnica complica el código un poco y no es posible en algunos lenguajes,
como Java y Python, que tienen recolección de basura pero provee una
implementación de la lista doblemente enlazada que requiere solamente un puntero
por nodo. Ver el artículo de revista de Sinha \cite{s04} para una discusión a
fondo de las lista XOR.

\begin{exc}
  ¿Por qué no es posible usar un nodo ficticio en una #SLList# para evitar que 
  todos los casos especiales que ocurren en las operaciones
  #push(x)#, #pop()#, #add(x)#, y #remove()#?
\end{exc}

\begin{exc}
  Diseña e implementa un método de #SLList#, #secondLast()#, que retorna 
  el penúltimo elemento de una #SLList#.  Haz esto sin usar la variable miembro
  , #n#, que mantiene un récord del tamaño de la lista.
\end{exc}

\begin{exc}
  Implementa las operaciones #get(i)#, #set(i,x)#,
  #add(i,x)# y #remove(i)# de la #List# en una #SLList#. Cada una de estas
  operaciones debe ejecutarse en tiempo $O(1+#i#)$.
\end{exc}

\begin{exc}
  Diseña e implementa un método de una #SLList#, #reverse()#, que invierte el
  orden de los elementos en una #SLList#.  Este método debe ejecutarse en tiempo
  $O(#n#)$, no debe usar recursión, no debe usar estructuras de datos
  secundarias, y no debe crear nodos nuevos.
\end{exc}

\begin{exc}
  Diseña e implementa los métodos de #SLList# y #DLList# llamados #checkSize()#.
  Estos métodos deben recorrer la lista y contar el número de nodos que deben 
  visitarse si este concuerda con el valor, #n#, almacenado en la lista. Estos
  métodos no retorna nada, pero arrojan una excepción si el tamaño que 
  calculan no concuerda con el valor de #n#.
\end{exc}

\begin{exc}
  Trata de recrear el código de la operación #addBefore(w)# que crea un nodo,
  #u#, y lo agrega en una #DLList# antes que el nodo #w#.  No utilices este
  capítulo como referencia. Aún si tu código no concuerda exactamente con el
  código en este libro podría todavía estar correcto. Ponlo a prueba y
  verifica si funciona.
\end{exc}

Los siguiente ejercicios involucran las manipulaciones sobre estructuras de
datos #DLList#. Deberías completarlos sin asignar nodos nuevos o arrays
temporarios. Todos pueden solucionarse al cambiar los valores de los nodos
#prev# and #next#.

\begin{exc}
  Escribe un método #isPalindrome()# de la #DLList# que retorna #true# si la
  lista es un \emph{palíndromo},
  \index{palíndromo}%
  i.e., el elemento en la posición #i# es igual al elemento
  en la posición $#n#-i-1$ para todos $i\in\{0,\ldots,#n#-1\}$.
  Tu código debería ejecutarse en tiempo $O(#n#)$.
\end{exc}

\begin{exc}
  Implementa un método #rotate(r)# que ``rota'' una #DLList# de tal manera
  que el artículo de la lista #i# se convierta en el artículo de lista
  $(#i#+#r#)\bmod #n#$.  Este método debería ejecutarse en tiempo
  $O(1+\min\{#r#,#n#-#r#\})$ y no debería modificar ningún nodo en la lista.
\end{exc}


\begin{exc}\exclabel{linkedlist-truncate}
  Escribe un método, #truncate(i)#, que trunca una #DLList# en la posición #i#.
  Después de ejecutar este método, el tamaño de la lista será #i# y debería
  solamente los elementos en los índices $0,\ldots,#i#-1$.  El valor de retorno es
  otra #DLList# que contiene los elementos en los índices 
  $#i#,\ldots,#n#-1$.  Este método debería ejecutarse en tiempo
  $O(\min\{#i#,#n#-#i#\})$.
\end{exc}

\begin{exc}
  Escribe un método, #absorb(l2)#, de una #DLList# que toman como un argumento
  otra #DLList#, #l2#, la vacía y agrega su contenido, en order, a la
  recipiente. Por ejemplo, si #l1# contiene $a,b,c$ y #l2#
  contiene $d,e,f$, después de haber llamado #l1.absorb(l2)#, #l1# contiene 
  $a,b,c,d,e,f$ y #l2# está vacía.
\end{exc}

\begin{exc}
  Escribe un método #deal()# que remueve todos los elementos con índices pares
  de una #DLList# y retorna #DLList# que contiene estos elementos.
  Por ejemplo, si #l1# contiene los elementos $a,b,c,d,e,f$, después de haber 
  llamado #l1.deal()#, #l1# debe contener $a,c,e$ y una lista que contiene 
  $b,d,f$ debería ser retornada.
\end{exc}

\begin{exc}
  Escribe una método, #reverse()#, que invierte el orden de los elementos en una 
  #DLList#.  
\end{exc}

\begin{exc}\exclabel{dllist-sort}
    \index{ordenamiento!por mezcla (merge-sort)}%
  Este ejercicio te guía a través de una implementación del algoritmo de
  \emph{ordenamiento por mezcla} para ordenar una #DLList#, como se discute en
  \secref{merge-sort}.

  \javaonly{In your implementation, perform comparisons between elements
  using the #compareTo(x)# method so that the resulting implementation can
  sort any #DLList# containing elements that implement the #Comparable#
  interface.}

  \begin{enumerate}
    \item Escribe un método de una #DLList# llamado #takeFirst(l2)#.
       Este método toma el primer nodo de #l2# y lo agrega a la lista
       recipiente. Esto es equivalente a #add(size(),l2.remove(0))#,
       excepto que no debería crear un nodo nuevo.
    \item Escribe método estático de #DLList#, #merge(l1,l2)#, que toma dos
       listas ordenadas #l1# and #l2#, las une, y retorna una lista nueva
       ordenada que contiene el resultado. Esto causa que #l1# y #l2# se vacíen
       en el proceso. Por ejemplo, si #l1# contiene $a,c,d$ y #l2# contiene
       $b,e,f$, entonces este método retorna una lista nueva que contiene
       $a,b,c,d,e,f$.
    \item Escribe un método de #DLList# llamado #sort()# que ordena los
        elementos contenidos en una lista usando el algoritmo de ordenamiento
        por mezcla.
        Este algoritmo recursivo funciona de la siguiente manera:
       \begin{enumerate}
          \item Si la lista contiene 0 o 1 elemento entonces no hay nada que
            hacer. De lo contrario,
          \item Usando el método #truncate(size()/2)#, divide la lista en dos
            listas de aproximadamente el mismo tamaño, #l1# y #l2#;
          \item Recursivamente ordena a #l1#;
          \item Recursivamente ordena a #l2#; y, finalmente,
          \item Une a #l1# y #l2# en una sola lista ordenada.
       \end{enumerate}
  \end{enumerate}
\end{exc}

Los siguientes ejercicios son más avanzados y requieren un buen entendimiento
de lo que sucede al valor mínimo almacenado en una #Stack#
o #Queue# a medida que se agregan o remueven artículos a dichas estructura de
datos. 

\begin{exc}
  \index{estructuras de datos!MinStack@#MinStack#}%
  Diseña e implementa una estructura de datos #MinStack# que almacena elementos
  comparables y soporta las operaciones #push(x)#,
  #pop()#, y #size()# de la pila, al igual que la operación #min()#,
  la cual retorna el valor mínimo actualmente almacenado en la estructura de
  datos. Todas las operaciones deben ejecutarse en tiempo constante.
\end{exc}

\begin{exc}
  \index{estructuras de datos!MinQueue@#MinQueue#}%
  Diseña e implementa una estructura de datos #MinQueue# que almacena elementos
  comparables y soporta las operaciones #add(x)#,
  #remove()#, y #size()# de la cola, al igual que la operación #min()#,
  la cual retorna el valor mínimo actualmente almacenado en la estructura de
  datos. Todas las operaciones deben ejecutarse en tiempo constante amortizado.
\end{exc}

\begin{exc}
  \index{estructuras de datos!MinDeque@#MinDeque#}%
  Diseña e implementa una estructura de datos #MinDeque# que almacena elementos
  comparables y soporta las operaciones #addFirst(x)#, #addLast()#, #removeLast()#
  y #size()# de la deque, al igual que la operación #min()#,
  la cual retorna el valor mínimo actualmente almacenado en la estructura de
  datos. Todas las operaciones deben ejecutarse en tiempo constante amortizado.
\end{exc}

Los ejercicios siguientes están diseñados para poner a prueba el entendimiento
de la implementación y el análisis de la #SEList# con eficiencia espacial:

\begin{exc}
  Demuestra que, si una #SEList# es usada como una #Stack# (de tal manera que la
  única modificación a la #SEList# se realiza usando $#push(x)#\equiv
  #add(size(),x)#$ y $#pop()#\equiv #remove(size()-1)#$), entonces estas
  operaciones se ejecutan en tiempo constante amortizado, 
  independiente del valor de #b#.
\end{exc}

\begin{exc}
  Diseña e implementa una versión de una #SEList# que soporta todas las
  operaciones de una #Deque# en tiempo constante amortizado por operación,
  independiente del valor de #b#.
\end{exc}

\begin{exc}
  Explica cómo usa el operador bitwise exclusive-or, \verb+^+, para
  intercambiar los valores de dos variables #int# usando una tercera variable.
\end{exc}





