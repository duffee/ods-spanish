\chapter{Montículos}
\chaplabel{heaps}

En este capítulo, discutimos dos implementaciones de las extremadamente útiles
estructuras de datos #Queue# de prioridad. Ambas estructuras tiene un tipo
especial de árbol binario conocido como un \emph{montículo}, 
\index{montículo}%
\index{montículo!binario}%
lo cual significa ``una pila desorganizada.''  Esto es en contraste a los
árboles binarios de búsqueda que pueden considerarse como una pila 
altamente organizada.

La primera implementación de un montículo usa un array para simular un árbol
binario completo. Esta implementación rápida es la base de uno de los más
conocidos algoritmos de ordenamiento, heapsort (ver \secref{heapsort}).
La segunda implementación está basada en árboles binarios más flexibles. 
La misma soporta la operación #meld(h)# que permite que la cola de prioridad 
sea capaz de absorber los elementos de otra cola de prioridad #h#.

\section{#BinaryHeap#: Un Árbol Binario Implícito}
\seclabel{binaryheap}

\index{estructuras de datos!BinaryHeap@#BinaryHeap#}%
Nuestra primera implementación de una estructura #Queue# (de prioridad) está
basada en una técnica que se remonta más de cuatro cientos años en el pasado.
El \emph{método de Eytzinger}
\index{método!de Eytzinger}%
nos permite representar un árbol binario completo como un array al colocar los
nodos del árbol en orden breadth-first (ver \secref{bintree:traversal}).
En esta manera, la raíz se almacena en la posición 0, el hijo izquierdo de la
raíz es almacenado en la posición 1, el hijo derecho de la raíz en la posición
2, el hijo izquierdo del hijo izquierdo de la raíz es almacenado en la posición
3, y así sucesivamente. Ver \figref{eytzinger}.

\begin{figure}
  \begin{center}
    \includegraphics[scale=0.90909]{figs/eytzinger}
  \end{center}
  \caption{El método de Eytzinger representa un árbol binario completo como un
    array.}
  \figlabel{eytzinger}
\end{figure}

Si aplicamos el método de Eytzinger a un árbol lo suficientemente grande, 
algunos patrones emergen. El hijo izquierdo  del nodo en el índice #i# se
encuentra en el índice $#left(i)#=2#i#+1$ y el hijo derecho del nodo en el
índice #i# se encuentra en el índice $#right(i)#=2#i#+2$. El padre del nodo 
en el índice #i# se encuentra en el índice $#parent(i)#=(#i#-1)/2$.
\codeimport{ods/BinaryHeap.left(i).right(i).parent(i)}

Una estructura #BinaryHeap# usa esta técnica para representar implícitamente 
un árbol binario completo en el cual los elementos están \emph{ordenados por
montículo}: 
\index{árbol!binario ordenano por montículo}%
\index{orden!por montículo}%
El valor almacenado en cualquier índice #i# no es menor que el valor en el
índice #parent(i)#, con la expresión del valor de la raíz, $#i#=0$. De ahí
se deduce que el valor más pequeño en la #Queue# (de prioridad) es por lo tanto
almacenado en la posición 0 (la raíz).

En una estructura #BinaryHeap#, los #n# elementos son almacenado en un array #a#:
\codeimport{ods/BinaryHeap.a.n}

Implementar la operación #add(x)# es relativamente trivial.  Como con todas
las estructuras basadas en array, primero verificamos que #a# esté llena (al
chequear si $#a.length#=#n#$) y, si lo está, hacemos crecer a #a#.  Siguiente,
colocamos a #x# en la posición #a[n]# e incrementamos a #n#.  En este punto,
todo lo que queda es asegurarnos que aún mantenemos la prioridad de montículo. 
Hacemos esto al intercambiar repetidamente a #x# con su padre hasta que #x#
no sea más pequeño que su padre.
Ver \figref{heap-insert}.
\codeimport{ods/BinaryHeap.add(x).bubbleUp(i)}

\begin{figure}
  \begin{center}
    \includegraphics[height=\QuarterHeightScaleIfNeeded]{figs/heap-insert-1} \\
    \includegraphics[height=\QuarterHeightScaleIfNeeded]{figs/heap-insert-2} \\
    \includegraphics[height=\QuarterHeightScaleIfNeeded]{figs/heap-insert-3} \\
    \includegraphics[height=\QuarterHeightScaleIfNeeded]{figs/heap-insert-4} \\
  \end{center}
  \caption[Añadiendo a una estructura BinaryHeap]{Añadiendo el valor 6 a una
    estructura #BinaryHeap#.}
  \figlabel{heap-insert}
\end{figure}

Implementar la operación #remove()#, la cual remueve el valor más pequeño 
del montículo, es un poco más difícil. Sabemos dónde el valor más pequeño se
encuentra (en la raíz), pero necesitamos reemplazarlo después que lo removemos
y aseguramos que la propiedad de montículo se mantiene.

La manera más fácil de hacer esto es reemplazar la raíz con el valor #a[n-1]#,
eliminar ese valor, y disminuir a #n#. Desafortunadamente, el nuevo elemento
raíz es ahora probablemente no el más pequeño, así que necesita moverse hacia
abajo. Hacemos esto al comparar repetidamente este elemento con su dos hijos.
Si es el más pequeño de los tres entonces hemos terminado. De lo contrario,
intercambiamos este elemento con el más pequeño de sus dos hijos y continuamos. 

\codeimport{ods/BinaryHeap.remove().trickleDown(i)}

\begin{figure}
  \begin{center}
    \includegraphics[height=\QuarterHeightScaleIfNeeded]{figs/heap-remove-1} \\
    \includegraphics[height=\QuarterHeightScaleIfNeeded]{figs/heap-remove-2} \\
    \includegraphics[height=\QuarterHeightScaleIfNeeded]{figs/heap-remove-3} \\
    \includegraphics[height=\QuarterHeightScaleIfNeeded]{figs/heap-remove-4} \\
  \end{center}
  \caption[Removiendo de una estructura BinaryHeap]{Removiendo el valor mínimo,
    4, de una estructura #BinaryHeap#.}
  \figlabel{heap-remove}
\end{figure}

Como con otras estructuras basadas en un array, ignoraremos el tiempo invertido 
en las llamadas a #resize()#, dado que estas pueden justificarse al usar el
argumento de amortización del \lemref{arraystack-amortized}. Los tiempos de
ejecución de ambas operaciones #add(x)# y #remove()# entonces dependen en la
altura del árbol binario (implícito). Afortunadamente, esto es un árbol binario
\emph{completo};
\index{árbol!binario completo}%
cada nivel excepto el último posee el máximo número posible de nodos. 
Por lo tanto, si la altura de este árbol es $h$, entonces tiene por lo menos 
$2^h$ nodos.
Puesto de otra manera
\[
  #n# \ge 2^h \enspace .
\]  
Tomando los logaritmos a ambos lados de esta ecuación resulta en
\[
   h \le \log #n# \enspace .
\]
Por lo tanto, ambas operaciones #add(x)# y #remove()# se ejecutan en tiempo 
$O(\log #n#)$.

\subsection{Resumen}

El teorema siguiente sintetiza el rendimiento de una estructura #BinaryHeap#:

\begin{thm}\thmlabel{binaryheap}
    Una estructura #BinaryHeap# implementa la interfaz #Queue# (de prioridad).
    Ignorando el costo de las llamadas a #resize()#, una estructura
    #BinaryHeap# soporta las operaciones #add(x)# y #remove()# en tiempo 
    $O(\log #n#)$ por operación.

    Además, iniciando con una estructura #BinaryHeap# vacía, cualquier secuencia
    de $m$ operaciones #add(x)# y #remove()# resulta en un total de $O(m)$
    tiempo investido durante todas las llamadas a #resize()#.
\end{thm}

\section{#MeldableHeap#: Un Montículo Meldable Aleatorizado}
\seclabel{meldableheap}

\index{estructuras de datos!MeldableHeap@#MeldableHeap#}%
En esta sección, describimos la estructura #MeldableHeap#, una implementación de
la estructura #Queue# (de prioridad) en la cual la estructura subyacente 
es también un árbol binario de ordenamiento por montículo. 

Sin embargo, a diferencia de una estructura #BinaryHeap# en la cual el árbol
binario subyacente es completamente definido por el número de elementos,
no existe restrición alguno en la forma del árbol binario que conforma 
una estructura #MeldableHeap#; todo se vale.

La operaciones #add(x)# y #remove()# en una estructura #MeldableHeap# se
implementan en términos de la operación #merge(h1,h2)#.  Esta operación
toma dos nodos montículos #h1# y #h2# y los fusiona, retornando un nodo montículo
que es la raíz de un montículo que contiene todos los elementos en el subárbol
arraigado en #h1# y todos los elementos en el subárbol arraigado en #h2#.

Un gran aspecto de una operación #merge(h1,h2)# es que puede ser definida 
recursivamente. Ver la \figref{meldable-merge}.  Si #h1# o #h2# es #nil#,
entonces estamos fusionando con un conjunto vacío, así que retornamos #h2#
o #h1#, respectivamente.  De lo contrario, asumimos que $#h1.x# \le #h2.x#$ dado
que, si $#h1.x# > #h2.x#$, entonces podemos invertir los rolos de #h1# and #h2#.
Entonces sabemos que la raíz del montículo fusionado contendrá a #h1.x#, y
podemos recursivamente fusionar a #h2# con #h1.left# o #h1.right#, según
deseemos. Este es la parte donde la aleatorización hace su entrada, y lanzamos 
una moneda para decidir si fusionamos a #h2# con #h1.left# o #h1.right#:
\codeimport{ods/MeldableHeap.merge(h1,h2)}

\begin{figure}
  \centering{\includegraphics[width=\ScaleIfNeeded]{figs/meldable-merge}}
  \caption[Fusionando en una estructura MeldableHeap]{Fusionando a #h1# y #h2#
    se hace al fusionar #h2# con #h1.left# o #h1.right#.}
  \figlabel{meldable-merge}
\end{figure}

En la sección siguiente, demostramos que #merge(h1,h2)# se ejecuta en tiempo
anticipado $O(\log #n#)$, donde #n# es el número total de elementos en #h1# y
#h2#.

Con acceso a la operación #merge(h1,h2)#, la operación #add(x)# es fácil. 
Creamos un nodo nuevo #u# que contiene a #x# y después fusionamos a #u#
con la raíz de nuestro montículo:
\codeimport{ods/MeldableHeap.add(x)}

Esto toma tiempo anticipado $O(\log (#n#+1)) = O(\log #n#)$.

La operación #remove()# es similarmente fácil. El nodo que queremos remover
es la raíz, así que solo fusionamos sus dos hijos y convertimos el resultado 
en la raíz: 
\codeimport{ods/MeldableHeap.remove()}


Nuevamente, esto toma tiempo anticipado $O(\log #n#)$.

Adicionalmente, una estructura #MeldableHeap# puede implementar otra operaciones
en tiempo anticipado $O(\log #n#)$, que incluyen:

\begin{itemize}
\item #remove(u)#: remueve el nodo #u# (y su llave #u.x#) del montículo.
\item #absorb(h)#: agrega todos los elementos de la estructura #MeldableHeap#
#h# al montículo, lo cual vacía a #h# en el proceso.
\end{itemize}

Cada una de estas operaciones puede implementarse usando un número constante de
operaciones #merge(h1,h2)# que toma tiempo anticipado  $O(\log #n#)$.

\subsection{Análisis de #merge(h1,h2)#}

El análisis de #merge(h1,h2)# está basado en el análisis de una caminata
aleatoria en un árbol binario. Una \emph{caminata aleatoria} en un árbol binario
comienza en la raíz del árbol. A cada paso en la caminata aleatoria, 
una moneda es lanzada y, dependiendo en el resultado de este lanzamiento de la
moneda, la caminata procede al hijo izquierdo o al hijo derecho del nodo 
actual. La caminata finaliza cuando se cae del árbol (en otras palabras,
el nodo actual se convierte en #nil#).

El lema siguiente es algo extraordinario porque no depende para nada en la forma
del árbol binario:

\begin{lem}\lemlabel{tree-random-walk}
    La longitud anticipada de una caminata aleatoria en un árbol binario con #n#
    nodos es como máxima #\log (n+1)#.
\end{lem}

\begin{proof}
    La demostración es por indución sobre #n#. En el caso base, $#n#=0$ y la
    caminata tiene longitud $0=\log (#n#+1)$.  Supón que el resultado es
    verdadero para todos los enteros que no son negativos $#n#'< #n#$.

    Sea $#n#_1$ el tamaño del subárbol izquierdo de la raíz, así que
    $#n#_2=#n#-#n#_1-1$ es el tamano del subárbol derecho de la raíz. 
    Comenzando en la raíz, la caminata toma un paso y después continúa con un
    subárbol de tamaño $#n#_1$ o $#n#_2$.  Por nuestra hipótesis inductiva, la
    longitud anticipada de la caminata es entonces
\[
    \E[W] = 1 + \frac{1}{2}\log (#n#_1+1) + \frac{1}{2}\log (#n#_2+1)  \enspace , 
\] 
debido a que $#n#_1$ y $#n#_2$ son menores que $#n#$.  Dado $\log$
es una función cóncava, $\E[W]$ es maximizada cuando $#n#_1=#n#_2=(#n#-1)/2$.
%To maximize this,
%over all choices of $#n#_1\in[0,#n#-1]$, we take the derivative and obtain
%\[
%    (\E[W])' = \frac{1}{2}(c/#n#_1 - c/(#n#-#n#_1-1)) \enspace , 
%\]
%which is equal to 0 when $#n#_1 = (#n#-1)/2$.  We can establish that
%this is a maximum fairly easily, so
Por lo tanto, el número anticipado de pasos tomado por la caminata aleatoria es
\begin{align*}
    \E[W] 
    & = 1 + \frac{1}{2}\log (#n#_1+1) + \frac{1}{2}\log (#n#_2+1) \\
   & \le  1 + \log ((#n#-1)/2+1) \\
   & =  1 + \log ((#n#+1)/2) \\
   & =  \log (#n#+1)  \enspace . \qedhere 
\end{align*}
\end{proof}

Hacemos una pequeña digresión para mencionar que, para los lectores que
saben un poco de la teoría de la información, la demostración del
\lemref{tree-random-walk} puede establecirse en términos de entropía. 

\begin{proof}[Información Teorérica Demostración de \lemref{tree-random-walk}]
    Sea $d_i$ la profundida del nodo externo $i$th y recuerda que un árbol binario 
    con #n# nodos tiene #n+1# nodos externos. La probabilidad que la caminata
    aleatoria alcance el nodo externo $i$th es exactamente
    $p_i=1/2^{d_i}$, así que la longitud anticipada de la caminata aleatoria
    es dada por

\[
   H=\sum_{i=0}^{#n#} p_id_i
    =\sum_{i=0}^{#n#} p_i\log\left(2^{d_i}\right)
    = \sum_{i=0}^{#n#}p_i\log({1/p_i})
\]

El lado derecho de esta ecuación es fácilmente reconocido como la entropía 
de una distribución probabilística sobre $#n#+1$ elementos. Un hecho básico
sobre la entropía de una distribución sobre $#n#+1$ elementos es que 
no excede $\log(#n#+1)$, lo cual demuestra el lema.
\end{proof}

Con este resultado sobre las caminatas aleatorias, podemos ahora fácilmente
demostrar que el tiempo de ejecución de la operación #merge(h1,h2)# es
$O(\log #n#)$.

\begin{lem}
    Si #h1# y #h2# son las raíces de los dos montículos que contienen
    $#n#_1$ y $#n#_2$ nodos, respectivamente, entonces el tiempo anticipado 
    de ejecución de #merge(h1,h2)# es como máximo $O(\log #n#)$, where $#n#=#n#_1+#n#_2$.
\end{lem}

\begin{proof}
    Cada paso del algoritmo de fusión toma un paso de una caminata aleatoria,
    en el montículo arraigado en #h1# o el montículo arraigado en #h2#.
    %, depending on whether $#h1.x# < #h2.x#$ or not.
    El algoritmo termina cuando una de las caminatas aleatorias se cae de su
    árbol correspondiente (cuando $#h1#=#null#$ or $#h2#=#null#$).
    Por lo tanto, el número anticipado de pasos realizado por el algoritmo
    de fusión es como máximo 
    \[
     \log (#n#_1+1) + \log (#n#_2+1) \le 2\log #n# \enspace . \qedhere
    \]
\end{proof}

\subsection{Resumen}

El teorema siguiente sintetiza el rendimiento de una estructura #MeldableHeap#:

\begin{thm}\thmlabel{meldableheap}
    Una estructura #MeldableHeap# implementa la interfaz  #Queue# (de
    prioridad). La misma estructura soporta las operaciones #add(x)# y
    #remove()# en tiempo anticipado $O(\log #n#)$ por operación.
\end{thm}

\section{Discusión y Ejercicios}

La representación implícita de un árbol binario completo como un array,
o lista, aparentement fue propuesta por primera vez por Eytzinger \cite{e1590}.
EL usó esta representación en libros que contienen árboles de lineaje familiar
\index{árbol!de lineaje familiar}%
de familias nobles. La estructura de datos #BinaryHeap# descrita aquí fue
introducida por primera vez por Williams \cite{w64}.

La estructura de datos #MeldableHeap# aleatorizada descrita aquí aparece
haber sido propuesta por primera vez por Gambin y Malinowski \cite{gm98}.
Otras implementaciones de montículos meldables existen, incluyendo 
montículos  de izquierda \cite[Section~5.3.2]{c72,k97v3},
\index{montículo!de izquierda}%
montículos binarios \cite{v78},
\index{montículo!binario}%
montículos Fibonacci \cite{ft87}, 
\index{montículo!Fibonacci}%
montículos de emparejamiento \cite{fsst86},\
\index{montículo!de emparejamiento}%
y montículos desequilibrados \cite{st83}, 
\index{montículo!desequilibrado}%
aunque ninguno de estos son tan simples como la estructura #MeldableHeap#

% HERE
Alguna de las estructuras anteriores también soportan una operación
#decreaseKey(u,y)#
\index{operación!decreaseKey@#decreaseKey(u,y)#}%
en la cual el valor almacenado en el nodo #u# se disminuye a #y#.  (Es una
precondición que $#y#\le#u.x#$.) En la mayoría de las estructuras anteriores,
esta operación puede sopotarse en tiempo $O(\log #n#)$ al remover el nodo #u#
#u# y añadir #y#.  Sin embargo, algunas de estas estructuras pueden implementar
#decreaseKey(u,y)# más eficientemente.  En particular, #decreaseKey(u,y)#
toma tiempo amortizado $O(1)$ en montículos de Fibonacci y 
tiempo amortizado $O(\log\log #n#)$ en una versión especial de montículos de
emparejamiento \cite{e09}.
Esta operación #decreaseKey(u,y)# más eficiente tiene aplicaciones en
en la aceleración de varios algoritmos sobre grafos, incluyendo 
el algoritmo de caminos mínimos de Dijkstra \cite{ft87}.

\begin{exc}
    Ilustra la adición de los valores 7 y después 3 a la estructura #BinaryHeap#
    mostrada al final de la \figref{heap-insert}.
\end{exc}

\begin{exc}
    Ilustra la remoción de los siguiente dos valores (6 y 8) de la estructura
    #BinaryHeap# que se muestra al final de la \figref{heap-remove}.
\end{exc}

\begin{exc}
    Implementa el método #remove(i)#, que remueve el valor almacenado en
    #a[i]# en una estructura #BinaryHeap#. Este método debería ejecutarse en
    tiempo  $O(\log #n#)$. Luego, explica por qué este método es posiblemente
    no tan útil. 
\end{exc}

\begin{exc}\exclabel{general-eytzinger}
  \index{árbol!$d$-ario}%
    Un árbol $d$-ario es una generalización de un árbol binario en el cual cada
    nodo interno $d$ hijos. Es posible utilizar el método de Eytzinger
    para representar árboles $d$-arios completos usando un array.  Tratar
    de calcular las ecuaciones que, dado un índice #i#, determinen el índice
    #i# de su padre y cada uno de los $d$ hijos de #i# en esta representación.
\end{exc}

\begin{exc}
  \index{estructuras de datos!DaryHeap@#DaryHeap#}%
    Usando lo que has aprendido en \excref{general-eytzinger}, diseña e
    implementa una estructura \emph{#DaryHeap#}, la generalización $d$-aria
    de una estructura #BinaryHeap#. Analiza los tiempos de ejecución de las
    operaciones sobre una estructura #DaryHeap# y examina el rendimiento
    de tu implementación de #DaryHeap# contra aquella de la implementación
    de #BinaryHeap# dada aquí.
\end{exc}

\begin{exc}
    Ilustra la adición de los valores 17 y después 82 a la estructura  
    #MeldableHeap# #h1# mostrada en \figref{meldable-merge}.
    Usa una moneda para simular un bit aleatorio cuando sea necesario.
\end{exc}

\begin{exc}
    Ilustra la remociṕn de los dos valores siguientes (4 y 8) en la estructura
    #MeldableHeap# #h1# que se muestra en la \figref{meldable-merge}.  Usa una
    moneda para simular un bit aleatorio cuando sea necesario.
\end{exc}

\begin{exc}
    Implementa el método #remove(u)#, que remueve el nodo #u# de una estructura
    #MeldableHeap#. Este método debería ejecutarse en tiempo anticipado
    $O(\log #n#)$.
\end{exc}

\begin{exc}
    Demuestra cómo encontrar el segundo valor más pequeño en una estructura 
    #BinaryHeap# o #MeldableHeap# en tiempo constante.
\end{exc}

\begin{exc}
    Demuestra cómo encontrar el valor $k$th más pequeño en una estructura 
    #BinaryHeap# o #MeldableHeap# en tiempo $O(k\log k)$.  (Pista: Usar otro
    montículo podría ser de ayuda.)
\end{exc}

\begin{exc}
    Supón que te proveen #k# listas ordenadas, de una longitud total #n#. 
    Usando un montícilo, demuestra cómo fusionar estas listas en un sola lista 
    ordenada en tiempo $O(n\log k)$.  (Pista: Comenzar con el caso $k=2$ puede
    ser instructivo.)
\end{exc}
