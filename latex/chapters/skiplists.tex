\chapter{Listas por saltos}

\chaplabel{skiplists}

En este capítulo, discutimos una estructura de datos hermosa: la lista por
saltos (\emph{ó} skiplist), la cual tiene una variedad de aplicaciones. 
Al usar una lista por saltos, podemos implementar una #List# que tiene
implementaciones de #get(i)#, #set(i,x)#, #add(i,x)#, y #remove(i)# de 
tiempo $O(\log n)$. También podemos implementar una #SSet# en la cual todas las
operaciones se ejecutan en tiempo anticipado $O(\log #n#)$.
%Finally, a skiplist can be used to implement a #Rope# in which all
%operations run in $O(\log #n#)$ time.

La eficiencia de las listas por saltos depende en el uso de la aleatorización.
Cuando un elemento nuevo es agregado a una lista por saltos, la lista por saltos
usa lanzamientos aleatorios de moneda para determinar la altura del elemento
nuevo. El rendimiento de las listas por saltos es expresado en términos de los
tiempos de ejecución anticipados y las longitudes de la ruta. Esta expectativa se
toma sobre los lanzamientos aleatorios de moneda usados por la lista por saltos.
En la implementación, los lanzamientos aleatorios de moneda usados por una lista
por saltos son simulados usando un generador de números (o bits) 
pseudo-aleatorios.

\section{La Estructura Básica}

\index{lista!por saltos}%
Conceptualmente, una lista por saltos es una secuencia de listas singularmente
enlazadas $L_0,\ldots,L_h$. Cada lista $L_r$ contiene un subconjunto de los
artículos en $L_{r-1}$.  Empezamos con la lista de entrada $L_0$ que contiene #n#
artículos y construye $L_1$ desde $L_0$, $L_2$ desde $L_1$, y así sucesivamente.
Los artículos en $L_r$ se obtienen al lanzar una moneda para cada elemento, #x#,
en $L_{r-1}$ e incluyendo #x# en $L_r$ si la moneda resulta en caras.
Este proceso termina cuando creamos una lista $L_r$ que es vacía. Un ejemplo
de una lista por saltos se muestra en la \figref{skiplist}.

\begin{figure}
  \begin{center}
    \includegraphics[width=\ScaleIfNeeded]{figs/skiplist}
  \end{center}
  \caption{Una lista por saltos que contiene siete elementos.}
  \figlabel{skiplist}
\end{figure}
% head -> cara
% tail -> cruz
Para un elemento, #x#, en una lista por saltos, conocemos la \emph{altura}
\index{altura!de una lista por saltos}%
de #x# como el valor $r$ más grande tal que #x# aparezca en $L_r$.  Así que,
por ejemplo, los elementos que solamente aparecen en $L_0$ tiene una altura
$0$.  Si pensamos un poco sobre la misma, notamos que la altura de #x#
corresponde al experimento siguiente: Lanza una moneda repetidamente hasta que 
aparezcan cruces. ¿Cuántas veces resultó en caras? No es de extrañar que la
respuesta sea que la altura anticipada de un nodo es 1. (Esperamos lanzar una
moneda dos veces antes de obtener cruces, pero no contamos el último
lanzamiento.) La \emph{altura} de una lista por saltos es la altura de su 
nodo más alto.


En la cabecera de cada lista hay un nodo especial, llamado el 
\emph{centinela} (\engterm{sentinel}),
\index{nodo!centinela}%
que actúa como un nodo ficticio para la lista. La propiedad esencial de las
listas por saltos es que existe una ruta corta, llamada la 
\emph{ruta de búsqueda}, 
\index{ruta!de búsqueda en una lista por saltos}%
desde el centinela $L_h$ a cada nodo en $L_0$. Recordar cómo construir una ruta de
búsqueda para un nodo, #u#, es fácil (ver la \figref{skiplist-searchpath}):
Empieza en la esquina superior izquierda de tu lista por saltos
(el centinela en $L_h$) y siempre dirígete hacia la derecha a menos que exceda 
a #u#, en cual caso debes tomar un espacio abajo hacia la lista inferior. 

Más precisamente, para construir la ruta de búsqueda para el nodo #u# en $L_0$,
comenzamos en el centinela, #w#, en $L_h$.  Siguiente, examinamos a #w.next#.
Si #w.next# contiene un artículo que aparece antes que #u# en $L_0$, entonces
asignamos $#w#=#w.next#$.  De lo contrario, descendemos y continuamos la
búsqueda en la ocurrencia de #w# en la lista $L_{h-1}$. Continuamos de esta
manera hasta que alcancemos el predecesor de #u# en $L_0$. 

\begin{figure}
  \begin{center}
    \includegraphics[width=\ScaleIfNeeded]{figs/skiplist-searchpath}
  \end{center}
  \caption{La ruta de búsqueda para el nodo que contiene a $4$ en una lista por
    saltos.}
  \figlabel{skiplist-searchpath}
\end{figure}

El resultado siguiente, el cual demostraremos en la \secref{skiplist-analysis},
demuestra que la ruta de búsqueda es bastante corta:

\begin{lem}\lemlabel{skiplist-searchpath}
La longitud anticipada de la ruta de búsqueda para cualquier nodo, #u#, en
$L_0$ es como máximo $2\log #n# + O(1) = O(\log #n#)$.
\end{lem}

Una manera espacial eficiente de implementar una lista por saltos es definir 
un nodo, #u#, que consiste de un valor, #x#, y un array, #next#, de punteros,
donde #u.next[i]# apunta al sucesor #u# en la lista $L_{#i#}$.  En esta manera,
el valor, #x#, en uno nodo es
\javaonly{referenced}\cpponly{stored}\pcodeonly{referenciado} 
solo una vez, aunque #x# puede aparecer en varias listas.

\javaimport{ods/SkiplistSSet.Node<T>}
\cppimport{ods/SkiplistSSet.Node}

Las siguientes dos secciones de este capítulo discuten dos aplicaciones
diferentes de las listas por saltos. En cada una de estas aplicaciones, 
$L_0$ almacena la estructura principal (una lista de elementos o un conjunto
ordenado de elementos). La diferencia principal entre estas estructuras 
es en cómo una ruta de búsqueda se navega; en particular, ellas difieren
en cómo deciden si una ruta de búsqueda debería ir hacia 
$L_{r-1}$ o directamente dentro de $L_r$.

\section{#SkiplistSSet#: Una #SSet# Eficiente}
\seclabel{skiplistset}

\index{estructuras de datos!SkiplistSSet@#SkiplistSSet#}%
Una estructura #SkiplistSSet# usa una estructura de lista por saltos para
implementar la interfaz #SSet#. Cuando se usa en esta manera, la lista $L_0$
almacena los elementos de la #SSet# de forma ordenada.  El método #find(x)#
funciona al seguir la ruta de búsqueda para el valor más pequeño #y# de tal
manera que $#y#\ge#x#$:

\codeimport{ods/SkiplistSSet.find(x).findPredNode(x)}

Seguir la ruta de búsqueda para #y# es fácil: cuando está situado en algún nodo,
#u#, en  $L_{#r#}$, buscamos directamente en #u.next[r].x#.
Si $#x#>#u.next[r].x#$, entonces tomamos un paso hacia la derecha
$L_{#r#}$; de lo contrario, nos movemos un paso abajo hacia $L_{#r#-1}$. Cada
paso (derecha o abajo) en esta búsqueda toma solamente tiempo constante; así
que, de acuerdo al \lemref{skiplist-searchpath}, el tiempo de ejecución
anticipado de #find(x)# es $O(\log #n#)$.

Antes de que podamos añadir un elemento a una #SkipListSSet#, necesitamos 
un método que simule los lanzamientos de una moneda para determinar la altura,
#k#, de un nodo nuevo. Hacemos esto al elegir un número entero aleatorio, #z#,
y contamos el número de $1$ colgantes en la representación binaria de #z#
\footnote{Este método no replica completamente el experimento de lanzar una
moneda dado que el valor de #k# siempre será menor que el número de bits en un
#int#. No obstante, esto tendrá un impacto insignificante a menos que el número
de elementos en la estructura sea mayor que $2^{32}=4294967296$.}:

\codeimport{ods/SkiplistSSet.pickHeight()}

Para implementar el método #add(x)# en una #SkiplistSSet# buscamos a #x#
y después empalmar (\engterm{splice}) #x# en varias listas $L_0$,\ldots,$L_{#k#}$, donde #k#
es seleccionado usando el método #pickHeight()#. La forma más fácil de hacer
esto es usar un array, #stack#, que mantiene un récord de los nodos en cual la
ruta de búsqueda desciende desde algunas listas 
$L_{#r#}$ into $L_{#r#-1}$. Más precisamente, #stack[r]# es el nodo en
$L_{#r#}$ donde la ruta de búsqueda procedió hacia abajo en 
$L_{#r#-1}$.  Los nodos que modificamos para insertar a #x#
son precisamente los nodos $#stack[0]#,\ldots,#stack[k]#$.  El siguiente código
implementa este algoritmo para #add(x)#:

\label{pg:skiplist-add}
\codeimport{ods/SkiplistSSet.add(x)}

\begin{figure}
  \begin{center}
    \includegraphics[width=\ScaleIfNeeded]{figs/skiplist-add}
  \end{center}
  \caption[Agregando una lista por saltos]{Agregando el nodo que contiene $3.5$
    a una lista por saltos. Los nodos almacenados en #stack# son resaltados.}
  \figlabel{skiplist-add}
\end{figure}

La remoción de un elemento, #x#, se hace en una manera similar, excepto que 
no hay necesidad de que #stack# mantenga un récord de la ruta de búsqueda. La
remoción puede hacerse a medida que seguimos la ruta de búsqueda. Buscamos a #x#
y en cada ocasión la búsqueda desciende de un nodo #u#, chequeamos 
si $#u.next.x#=#x#$ y si lo es, empalmamos a #u# de la lista:
\codeimport{ods/SkiplistSSet.remove(x)}

\begin{figure}
  \begin{center}
    \includegraphics[width=\ScaleIfNeeded]{figs/skiplist-remove}
  \end{center}
  \caption{Removiendo el nodo que contiene $3$ de una lista por saltos.}
  \figlabel{skiplist-remove}
\end{figure}

\subsection{Resumen}

El teorema siguiente sintetiza el rendimiento de las lista por saltos cuando se
usa para implementar cojuntos ordenados:

\begin{thm}\thmlabel{skiplist}
Una #SkiplistSSet# implementa la interfaz #SSet#. Una #SkiplistSSet# soporta
las operaciones #add(x)#, #remove(x)#, y #find(x)# en tiempo anticipado 
$O(\log #n#)$ por operación.
\end{thm}

\section{#SkiplistList#: Una #List# con Accesso Aleatorio Eficiente}
\seclabel{skiplistlist}

\index{estructuras de datos!SkiplistList@#SkiplistList#}%
Una #SkiplistList# implementa la interfaz #List# usando una estructura lista
por saltos. En una #SkiplistList#, $L_0$ contiene los elementos de la lista en
el orden el cual los mismos aparecen en la lista. Como en una #SkiplistSSet#,
los elementos pueden agregarse, removerse, y accederse en tiempo $O(\log#n#)$.

Para que esto sea posible, necesitamos una manera de seguir la ruta de búsqueda
para el elemento $#i#\ulo$ en $L_0$. La forma más fácil de hacer esto es definir
la noción de la \emph{longitud} de una una arista en alguna lista, $L_{#r#}$.
Definimos la longitud de cada arista en $L_{0}$ como 1.  La longitud de una
arista, #e#, en $L_{#r#}$, $#r#>0$, es definida como la suma de las longitudes
de las aristas debajo de #e# en $L_{#r#-1}$.  De manera equivalente, la
longitud de #e# es el número de aristas en $L_0$ debajo de #e#.  Observa la 
\figref{skiplist-lengths} para un ejemplo que muestra las longitudes de las
aristas de una lista por saltos. Dado que las aristas de las listas por saltos
se almacenan en arrays, las longitudes pueden ser almacenadas de manera similar:

\begin{figure}
  \begin{center}
    \includegraphics[width=\ScaleIfNeeded]{figs/skiplist-lengths}
  \end{center}
  \caption{Las longitudes de las aristas en una lista por saltos.}
  \figlabel{skiplist-lengths}
\end{figure}

\javaimport{ods/SkiplistList.Node}
\cppimport{ods/SkiplistList.Node}

La propiedad útil de esta definición de la longitud es que, si estamos
actualmente en un nodo que se encuentra en la posición #j# en $L_0$ y seguimos
una arista de longitud $\ell$, entonces nos movemos a un nodo cuya posición, 
en $L_0$, es $#j#+\ell$.  De esta manera, mientras seguimos una ruta de
búsqueda, mantenemos un récord de la posición, #j#, del nodo actual en 
$L_0$.  Cuando estamos en un nodo, #u#, en $L_{#r#}$, nos dirigimos hacia la
derecha si #j# más la longitud la longitud de la arista #u.next[r]# es menor que
#i#. De lo contrario, nos dirigimos abajo hacia $L_{#r#-1}$.

\codeimport{ods/SkiplistList.findPred(i)}
\codeimport{ods/SkiplistList.get(i).set(i,x)}

Dado que la parte más difícil de las operaciones #get(i)# y #set(i,x)# es 
encontrar el nodo $#i#\ulo$ en $L_0$, estas operaciones se ejecutan en 
tiempo $O(\log #n#)$.

Añadir un elemento a una #SkiplistList# en una posición, #i#, es relativamente
fácil. A diferencia de una #SkiplistSSet#, estamos seguros que un nodo nuevo 
será actualmente añadido, así que podemos hacer la adición al mismo tiempo 
que buscamos la posición del nuevo nodo. Primero elegimos la altura, #k#, 
del nodo recién insertado, #W#, y después seguimos la ruta de búsqueda para #i#.
En cualquier momento que la ruta de búsqueda se mueve desciende desde
$L_{#r#}$ con $#r#\le #k#$, empalma a #w# en $L_{#r#}$.  La única
precaución extra que debemos tomar es asegurar que las longitudes de las aristas
se actualicen apropiadamente. Ver la \figref{skiplist-addix}.

\begin{figure}
  \begin{center}
    \includegraphics[width=\ScaleIfNeeded]{figs/skiplist-addix}
  \end{center}
  \caption[Agregando a una SkiplistList]{Agregando un elemento a una #SkiplistList#.}
  \figlabel{skiplist-addix}
\end{figure}

Nota que, cada vez que la ruta de búsqueda desciende a un nodo, #u#, en 
$L_{#r#}$, la longitudes de la arista #u.next[r]# incrementa por uno,
debido a que estamos agregando un elemento debajo de esa arista en la posición
#i#. El empalme del nodo #w# entre dos nodos,
#u# and #z#, funciona de la manera en que se muestra en la
\figref{skiplist-lengths-splice}.  Mientras seguimos la ruta de búsqueda ya
estamos manteniendo un récord de la posición, #j#, de #u# en 
$L_0$.  Por lo tanto, sabemos que la longitud de la arista desde 
#u# a #w# es $#i#-#j#$.  Podemos también deducir la longitud de la arista desde
#w# a #z# de la longitud,$\ell$, de la arista desde #u# a #z#. Por lo tanto, 
podemos empalmar a #w# y actualizar las longitudes de las aristas en 
tiempo constante.

\begin{figure}
  \begin{center}
    \includegraphics[scale=0.90909]{figs/skiplist-lengths-splice}
  \end{center}
  \caption[Agregando a una SkiplistList]{Actualizando las longitudes de las
    aristas mientras se empalma a un nodo #w# en una lista por saltos.
   }
  \figlabel{skiplist-lengths-splice}
\end{figure}

Esto suena más complicado de lo que realmente es, dado que el código es
actualmente bastante simple:

\codeimport{ods/SkiplistList.add(i,x)}
\codeimport{ods/SkiplistList.add(i,w)}

A esta altura, la implementación de la operación #remove(i)# en una 
#SkiplistList# debería ser obvia. Seguimos la ruta de búsqueda para el nodo en
la posición #i#. Cada vez que la ruta de búsqueda toma un paso hacia abajo desde
un nodo, #u#, al nivel #r# disminuimos la longitud de la arista que deja a #u# a
ese nivel. Igualmente chequeamos si #u.next[r]# es el elemento de rango #i# y,
si lo es, lo empalmamos de la lista a ese nivel. Un ejemplo se muestra en
la \figref{skiplist-removei}.

\begin{figure}
  \begin{center}
    \includegraphics[width=\ScaleIfNeeded]{figs/skiplist-removei}
  \end{center}
  \caption[Removiendo un elemento de una SkiplistList]{Removiendo un elemento
    desde una #SkiplistList#.}
  \figlabel{skiplist-removei}
\end{figure}
\codeimport{ods/SkiplistList.remove(i)}

\subsection{Resumen}

El teorema siguiente sintetiza el rendimiento de la estructura de datos 
#SkiplistList#:

\begin{thm}\thmlabel{skiplistlist}
  Una #SkiplistList# implementa la interfaz #List#.
  Una  #SkiplistList# soporta las operaciones #get(i)#, #set(i,x)#, #add(i,x)#,
  y #remove(i)# en tiempo anticipado $O(\log #n#)$ por operación.
\end{thm}

%\section{Skiplists as Ropes}
%TODO: A section on ropes

\section{Análisis de las Listas por Saltos}
\seclabel{skiplist-analysis}

En esta sección, analizamos la altura anticipada, tamaño, y longitud de la ruta
de búsqueda en una lista por saltos. Esta sección requiere conocimiento de
probabilidad básica. Varias demostraciones se basan en la siguiente observación
de los lanzamientos de una moneda.

\begin{lem}\lemlabel{coin-tosses}
  \index{moneda!lanzamiento de}%
  Sea $T$ el número de veces que una moneda justa se lanza e incluyendo
  la primera vez que la moneda resultó en cara. Entonces $\E[T]=2$.
\end{lem}

\begin{proof}
  Supón que paramos de lanzar la moneda la primera vez que resulta en cara. 
  Define la variable indicadora
  \[ I_{i} = \left\{\begin{array}{ll}
     0 & \mbox{si la moneda se lanza menos $i$ veces} \\
     1 & \mbox{si la moneda se lanza $i$ o más veces} 
     \end{array}\right.
  \]
  Nota que $I_i=1$ si y solo si los primeros $i-1$ lanzamientos de moneda
  resultan en escudos, así que $\E[I_i]=\Pr\{I_i=1\}=1/2^{i-1}$.  Observa que
  $T$, el número total de lanzamientos, puede escribirse como
  $T=\sum_{i=1}^{\infty} I_i$. Por lo tanto,
  \begin{align*}
    \E[T] & =  \E\left[\sum_{i=1}^\infty I_i\right] \\
     & =  \sum_{i=1}^\infty \E\left[I_i\right] \\
     & =  \sum_{i=1}^\infty 1/2^{i-1} \\
     & =  1 + 1/2 + 1/4 + 1/8 + \cdots \\
     & =  2 \enspace .   \qedhere
  \end{align*} 
\end{proof}

Los dos lemas siguientes nos informan que las listas por saltos tienen tamaño
linear:

\begin{lem}\lemlabel{skiplist-size1}
  El número anticipado de nodos en una lista por saltos que contiene 
  $#n#$ elementos, sin incluir las ocurrencias del centinela, 
  es $2#n#$.
\end{lem}

\begin{proof}
  La probabilidad que cualquier elemento particular, #x#, esté incluido en la
  lista $L_{#r#}$ es $1/2^{#r#}$, así que el número anticipado de nodos en $L_{#r#}$
  es $#n#/2^{#r#}$.\footnote{Referirte a la \secref{randomization} para ver cómo esto se
    deriva al usar variables indicadoras y el valor esperado.}
  Por lo tanto, el número total anticipado de nodos en todas las listas es
  \[ \sum_{#r#=0}^\infty #n#/2^{#r#} = #n#(1+1/2+1/4+1/8+\cdots) = 2#n# \enspace . \qedhere \]
\end{proof}

\begin{lem}\lemlabel{skiplist-height}
  La altura anticipada de una lista por saltos que contiene #n# elementos es
  como máximo $\log #n# + 2$.
\end{lem}

\begin{proof}
  Por cada $#r#\in\{1,2,3,\ldots,\infty\}$, 
  define la variable indicadora aleatoria
  \[ I_{#r#} = \left\{\begin{array}{ll}
     0 & \mbox{si $L_{#r#}$ está vacía} \\
     1 & \mbox{if $L_{#r#}$ si no está vacía}
     \end{array}\right.
  \]
  La altura, #h#, de la lista por saltos es entonces dada por
  \[
       #h# = \sum_{r=1}^\infty I_{#r#} \enspace .
  \]
  Nota que $I_{#r#}$ es nunca mayor que la longitud, $|L_{#r#}|$, de $L_{#r#}$,
  así que
  \[
     \E[I_{#r#}] \le \E[|L_{#r#}|] = #n#/2^{#r#} \enspace .
  \]
  Por lo tanto, tenemos que 
  \begin{align*}
       \E[#h#] &= \E\left[\sum_{r=1}^\infty I_{#r#}\right] \\
        &= \sum_{#r#=1}^{\infty} E[I_{#r#}] \\
        &= \sum_{#r#=1}^{\lfloor\log #n#\rfloor} E[I_{#r#}]
                 + \sum_{r=\lfloor\log #n#\rfloor+1}^{\infty} E[I_{#r#}]  \\
        &\le \sum_{#r#=1}^{\lfloor\log #n#\rfloor} 1
                 + \sum_{r=\lfloor\log #n#\rfloor+1}^{\infty} #n#/2^{#r#} \\
        &\le \log #n#
                 + \sum_{#r#=0}^\infty 1/2^{#r#} \\
        &= \log #n# + 2 \enspace . \qedhere
  \end{align*}
\end{proof}

\begin{lem}\lemlabel{skiplist-size2}
  El número anticipado de nodos en una lista por saltos que contiene $#n#$
  elementos, incluyendo todas las ocurrencias del centinela, $2#n#+O(\log #n#)$.
\end{lem}

\begin{proof}
  Por el \lemref{skiplist-size1}, el número anticipado de nodos, excluyendo el
  centinela, es $2#n#$.  El número de ocurrencias del centinela es igual a la
  altura, $#h#$, de la lista por saltos así que, por el
  \lemref{skiplist-height}, el número anticipado de ocurrencias del centinela
  es como máximo $\log #n#+2 = O(\log #n#)$.
\end{proof}


\begin{lem}
La longitud anticipada de una ruta de búsqueda en una lista por saltos es como
máximo $2\log #n# + O(1)$.
\end{lem}

\begin{proof}
  La manera más fácil de proceder con esto es considerar la 
  \emph{ruta de búsqueda inversa} para un nodo, #x#. Esta ruta empieza en el
  predecesor de #x# en $L_0$.  Durante cualquier momento, si la ruta puede 
  puede subir un nivel, entonces lo hace. Si no puede subir un nivel, entonces
  se dirige hacia la izquierda. Si pensamos sobre esto por un momento, nos
  convenceremos que la ruta de búsqueda inversa para #x# es idéntica a la ruta
  de búsqueda para #x#, excepto que es inversa.

  El número de nodos que la ruta de búsqueda inversa visita en un nivel
  particular, #r#, está relacionado al experimento siguiente: Lanza una
  moneda. Si la moneda resulta en cara, entonces muévete hacia arriba y para.
  De lo contrario, muévete a la izquierda y repite el experimento. El número
  de lanzamientos de la moneda antes que resulte en cara representa el número
  de pasos a la izquierda que una ruta de búsqueda inversa toma en un nivel
  particular. \footnote{Nota que esto podría contar en exceso el número de pasos
  a la izquierda, dado que el experimento debería terminar con la primera cara
  o cuando la ruta de búsqueda alcanza el centinela, cualquiera de los dos
  ocurra primero. Esto no es un problema debido a que el lema solamente habla
  de un límite superior.} El \lemref{coin-tosses} nos dice que el número
  anticipado de lanzamientos de una moneda antes que resulte en cara es 1.

  Denotemos a $S_{#r#}$ como el número de pasos que la ruta de búsqueda directa
  toma a un nivel $#r#$ que se dirige hacia la derecha. Recientemente hemos
  argumentado que $\E[S_{#r#}]\le 1$.  Además, $S_{#r#}\le |L_{#r#}|$, debido
  a que no podemos tomar más pasos en $L_{#r#}$ que la longitud de $L_{#r#}$,
  así que 
  \[
    \E[S_{#r#}] \le \E[|L_{#r#}|] = #n#/2^{#r#} \enspace .
  \]

  Podemos ahora finalizar como en la demostración del 
  \lemref{skiplist-height}.

  Sea $S$ la longitud de la ruta de búsqueda para un algún nodo, #u#, en una
  lista por saltos, y sea $#h#$ la altura de la lista por saltos. Entonces
  \begin{align*}
      \E[S] 
         &= \E\left[ #h# + \sum_{#r#=0}^\infty S_{#r#} \right] \\
         &= \E[#h#] + \sum_{#r#=0}^\infty \E[S_{#r#}]  \\
         &= \E[#h#] + \sum_{#r#=0}^{\lfloor\log #n#\rfloor} \E[S_{#r#}] 
              + \sum_{#r#=\lfloor\log #n#\rfloor+1}^\infty \E[S_{#r#}] \\
         &\le \E[#h#] + \sum_{#r#=0}^{\lfloor\log #n#\rfloor} 1
              + \sum_{r=\lfloor\log #n#\rfloor+1}^\infty #n#/2^{#r#} \\
         &\le \E[#h#] + \sum_{#r#=0}^{\lfloor\log #n#\rfloor} 1
              + \sum_{#r#=0}^{\infty} 1/2^{#r#} \\
         &\le \E[#h#] + \sum_{#r#=0}^{\lfloor\log #n#\rfloor} 1
              + \sum_{#r#=0}^{\infty} 1/2^{#r#} \\
         &\le \E[#h#] + \log #n# + 3 \\
         &\le 2\log #n# + 5  \enspace . \qedhere
  \end{align*}
\end{proof}

El teorema siguiente sintetiza los resultados en esta sección:

\begin{thm}
Una lista por saltos que contiene $#n#$ elementos tiene un tamaño anticipado 
$O(#n#)$ y la longitud anticipada de la ruta de búsqueda para un elemento en
particular es como máximo $2\log #n# + O(1)$.
\end{thm}


%\section{Iteration and Finger Search}

%TODO: Write this section

\section{Discusión y Ejercicios}

Las lista por saltos fueron introducidas por Pugh \cite{p91} quien también
presentó un número de aplicaciones y extensiones de las listas por saltos 
\cite{p89}.  Desde aquel entonces, ellas han sido estudiadas extensivamente.
Varios investigadores han hecho análisis precisos de la longitud anticipada y la
varianza de la longitud de la ruta de búsqueda para el elemento $#i#\ulo$ en una
lista por saltos \cite{kp94,kmp95,pmp92}. Versiones determinista \cite{mps92},
parciales \cite{bbg02,esss01}, y auto-ajustable \cite{bdl08} de las lista por
saltos han sido todas desarrolladas. Implementaciones de las listas por
saltos se han escrito para varios lenguajes y \emp{frameworks} y han sido usadas
en sistemas de base de datos de código abierto \cite{skipdb,redis}. Una variante
de las listas por saltos se usa en las estructuras de gestión de procesos 
del kernel del sistema operativo HP-UX \cite{hpux}.
\javaonly{Skiplists are even part of the Java 1.6 API \cite{oracle_jdk6}.}

\begin{exc}
    Ilustra las rutas de búsqueda para 2.5 y 5.5 sobre la lista por saltos en la
    \figref{skiplist}.
\end{exc}

\begin{exc}
    Ilustra la adición del 0.5 (con una altura de 1) y después de 3.5
    (con una altura de 2) a la lista por saltos en la \figref{skiplist}.
\end{exc}

\begin{exc}
    Ilustra la remoción del 1 y después 3 sobre la lista por saltos
    en la \figref{skiplist}.
\end{exc}

\begin{exc}
    Ilustra la ejecución de #remove(2)# sobre la #SkiplistList#
    en la \figref{skiplist-lengths}.
\end{exc}

\begin{exc}
    Ilustra la ejecución  de #add(3,x)# sobre la #SkiplistList# en la
    \figref{skiplist-lengths}.  Supón que #pickHeight()# selecciona una altura
    de 4 para el nodo creado.
\end{exc}

\begin{exc}\exclabel{skiplist-changes}
    Demuestra que, durante una operación #add(x)# o #remove(x)#, el número
    anticipado de punteros en una #SkiplistSet# que cambia es constante.
\end{exc}

\begin{exc}\exclabel{skiplist-opt}
    Supón que, en lugar de promover un elemento desde 
    $L_{i-1}$ hacia $L_i$ basado en un lanzamiento de moneda, lo promovemos con
    alguna probabilidad $p$, $0 <  p < 1$.
  \begin{enumerate}
   \item Demuestra que, con esta modificación, la longitud anticipada de una
       ruta de búsqueda es como máxima $(1/p)\log_{1/p} #n# + O(1)$.
   \item ¿Cuál es el valor de $p$ que minimiza la expresión anterior?
   \item ¿Cuál es el valor anticipado de la altura de una lista por saltos?
   \item ¿Cuál es el número anticipado de nodos en la lista por saltos?
  \end{enumerate}
\end{exc}

\begin{exc}\exclabel{skiplist-opt-2}
    El método #find(x)# en una #SkiplistSet# algunas veces realiza
  \emph{comparaciones redundantes}; esto ocurre cuando #x# es comparada
  al mismo valor más de una vez. Estas comparaciones pueden ocurrir cuando, para
  algún nodo, #u#, $#u.next[r]# = #u.next[r-1]#$.  Demuestra que estas
  comparaciones redundantes ocurren y modifica a #find(x)# para evitarlas.
  Analiza el número anticipado de comparaciones hechas por tu método 
  #find(x)# modificado.
\end{exc}

\begin{exc}
  Diseña e implementa una versión de una lista por saltos que implementa la
  interfaz #SSet#, pero que también permite acceso rápido 
  a los elementos por rango. Es decir, la estructura también soporta la función
  #get(i)#, la cual retorna un elemento cuyo rango es #i# en tiempo anticipado 
  $O(\log #n#)$. (El rango de un elemento #x# en una #SSet# es el número de
  elementos en #SSet# que son números que #x#.)
\end{exc}

\begin{exc}
  \index{dedo}%
  \index{búsqueda!de dedo en una lista por saltos}%
  Un \emph{dedo} en una lista por saltos es una array que almacena  la secuencia
  de nodos en una ruta de búsqueda en la cual la ruta de búsqueda desciende.
  (La variable #stack# en el código de #add(x)# en la 
  página~\pageref{pg:skiplist-add} es un dedo;  el nodo sombreado en la
  \figref{skiplist-add} muestra el contenido del dedo.)  Podemos pensar de un 
  dedo como indicando el ruta a un nodo en la lista más baja, $L_0$.

  Una \emph{búsqueda de dedo} implementa la operación #find(x)# usando un dedo,
  al recorrer la lista hacia arriba usando el dedo hasta alcanzar un nodo #u#
  tal que $#u.x# < #x#$ y $#u.next#=#null#$ o $#u.next.x# >
  #x#$ y subsecuentemente realizar una búsqueda normal para #x# comenzando
  desde #u#. Es posible demostrar que el número anticipado de pasos requeridos
  para una búsqueda de dedo es $O(1+\log r)$, donde $r$ es el valor numérico en
  $L_0$ entre #x# y el valor señalado por el dedo.

  Implementa una subclase de #Skiplist# llamada #SkiplistWithFinger# que
  implementa las operaciones #find(x)# usando un dedo interno. Esta subclase
  almacena un dedo, el cual es entonces usado de tal manera que cada operación
  #find(x)# es implementada como una búsqueda de dedo. Durante cada operación
  #find(x)# el dedo es actualizado de tal forma que cada operación #find(x)#
  usa, como un punto de partido, un dedo que señala el resultado de la
  operación #find(x)# previa.
\end{exc}

\begin{exc}\exclabel{skiplist-truncate}
  Escribe un método, #truncate(i)#, que trunca una #SkiplistList#
  en la posición #i#. Después de la ejecución de este método, el tamaño 
  de la lista es #i# y contiene solamente los elementos en los índices
  $0,\ldots,#i#-1$.  El valor de retorno es otra  #SkiplistList# que contiene
  los elementos en los índices $#i#,\ldots,#n#-1$.  Este método
  debería ejecutarse en tiempo $O(\log #n#)$.
\end{exc}

\begin{exc}
  Escribe un método de una #SkiplistList#, llamado #absorb(l2)#, que toma como
  un argumento una #SkiplistList#, #l2#, lo vacía y agrega su contenido,
  en orden, al recipiente.  Por ejemplo, si #l1# contiene $a,b,c$
  y #l2# contiene $d,e,f$, entonces después de invocar a #l1.absorb(l2)#, #l1#
  contendrá $a,b,c,d,e,f$ y #l2# estará vacía. Este método debe ejecutarse en
  tiempo $O(\log #n#)$.
\end{exc}

\begin{exc}
  Usando las ideas de la lista con eficiencia espacial, #SEList#, diseña e implementa 
  una #SSet# con eficiencia espacial, #SESSet#.  Para hacerlo, almacena los
  datos, en orden, en una #SEList#, y almacen los bloques de esta #SEList#
  en una #SSet#. Si la implementación original de #SSet# usa un espacio $O(#n#)$
  para almacenar #n# elementos, entonces la #SESSet# usará espacio suficiente 
  para #n# elementos más $O(#n#/#b#+#b#)$ espacio malgastado.
\end{exc}

\begin{exc}
  Usando una #SSet# como su estructura básica, diseña e implementa una
  aplicación que lee un archivo de texto (grande) y te permite buscar,
  interactivamente, cualquier subcadena de texto en el texto. A medida que el
  usuario teclea su búsqueda, una parte del texto que coincide (si alguna lo
  hace) debería aparecer como resultado.

  \noindent  Pista 1: Cada subcadena es un prefijo de algún sufijo, así que es
  suficiente almacenar todos los sufijos del archivo de texto.

  \noindent Pista 2:  Cualquier sufijo puede representarse compactamente como un
  solo entero que indica donde el sufijo comienza en el texto.

  \noindent Prueba tu aplicación con textos grandes, tales como los libros
  disponibles Project Gutenberg \cite{gutenberg}.  Si se hace correctamente, tu
  aplicación será muy responsiva; no debería haber retraso notable entre 
  el tecleo de los caracteres y los resultados siendo mostrados.
\end{exc}

\begin{exc}
  \index{lista!por saltos versus árbol binario de búsqueda}%
  \index{árbol!binarrio de búsqueda versus lista por saltos}%
  (Este debería hacerse después de haber leído los árboles binarios de búsqueda,
  en la \secref{binarysearchtree}.)  Compara las listas por saltos con los
  árboles binarios de búsqueda en las siguientes maneras:
  \begin{enumerate}
     \item Explica cómo la remoción de algunas aristas de una lista por saltos
     conduce a una estructura parecida a un árbol binario y es similar a un
     árbol binario de búsqueda.
     \item Las listas por saltos y los árboles binarios de búsqueda usan cada uno
     un número de punteros (2 por nodo). Aunque, las listas por saltos hacen un
     mejor uso de dichos punteros. Explicar por qué.
  \end{enumerate}
\end{exc}
