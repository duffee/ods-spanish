\chapter{Árboles Binarios}
\chaplabel{binarytrees}

Este capítulo introduce una de las estructuras más fundamentales en la ciencia
de la computación: árboles binarios. El uso de la palabra
\emph{árbol}
\index{árbol}%
\index{árbol!binario}%
aquíe proviene del hecho que, cuando los dibujamos, el dibujo resultante
usualmente asemeja los árboles en los bosques. Existen muchas maneras de 
definir los árboles binarios. Matemáticamente, un 
\emph{árbol binario} es un grafo finito, no dirigido y conectado
sin ciclos y sin vértices cuyo grado sea mayor que 3.

Para la mayoría de las aplicaciones de la ciencia de la computación, los 
árboles binarios son \emph{arraigados:}
\index{árbol!arraigado}%
%\index{rooted tree}%
Un nodo especial, #r#, de grado dos como máximo es llamado la \emph{raíz}
del árbol. Por cada nodo, $#u#\neq #r#$, el segundo node en la ruta
desde #u# a #r# es conocido como el \emph{padre} de #u#.
\index{padre}%
Cada uno de los otros nodos adyacentes a #u# es llamado un \emph{hijo}
\index{hijo} de #u#. La mayoría de los árboles binarios en los cuales estamos
interasados son \emph{ordenados},
%\index{ordered tree}%
\index{árbol!ordenado}%
así que diferenciamos entre el 
\emph{hijo izquierdo} y el \emph{hijo derecho} de #u#.
%\index{left child}%
\index{hijo!izquierdo}%
%\index{right child}%
\index{hijo!derecho}%

En ilustraciones, los árboles binarios se dibujan usualmente desde la raíz 
hacia abajo, con la raíz en la parte superior del dibujo y los hijos izquierdos
y derechos dados respectivamente por las posiciones izquierda y derecha en
el dibujo (\figref{bintree-orientation}).  Por ejemplo,
\figref{binary-tree}.a muestra un árbol binario con nueve nodos.

\begin{figure}
  \begin{center}
    \includegraphics[scale=0.90909]{figs/bintree-traverse-1} 
  \end{center}
  \caption[Padre, hijo izquierdo e hijo derecho]{El padre, hijo izquierdo, e
    hijo derecho del nodo #u# en una #BinaryTree#.}
  \figlabel{bintree-orientation}
\end{figure}


\begin{figure}
  \begin{center}
    \begin{tabular}{cc}
      \includegraphics[width=\HalfScaleIfNeeded]{figs/bintree-1} &
      \includegraphics[width=\HalfScaleIfNeeded]{figs/bintree-2} \\
      (a) & (b)
    \end{tabular}
  \end{center}
  \caption{A binary tree with (a)~nine real nodes and (b)~ten external nodes.}
  \figlabel{binary-tree}
\end{figure}

Debido a que los árboles binarios son importantes, ciertos términos se han 
desarollado para describir a los mismos:

La \emph{profundidad}
\index{profundidad}%
de un nodo, #u#, en árbol binario es la longitud del recorrido desde #u# a 
la raíz del árbol. Si un nodo, #w#, se encuentra en el recorrido desde 
#u# a #r#, dicho nodo se conoce como un \emph{ancestro} de
\index{ancestro}%
de #u# y #u# un \emph{descendiente}
\index{descendiente}%
de #w#.  El \emph{subárbol} de un 
nodo, #u#, es el árbol binario arraigado en #u# y que contien todos los
decendientes de #u#. La \emph{altura}
\index{árbol!altura en un} de un nodo, #u#, es la longitud
del recorrido más largo desde #u# a uno de sus descendientes. La \emph{altura}
\index{árbol!altura de un}%
de un árbol es la altura de su raíz.
Un nodo, #u#, es una \emph{hoja}
\index{hoja}%
si no tiene hijos.

Algunas veces consideramos a los árboles como siendo incrementados con
\emph{nodos externos}. Cualquier nodo que no tenga un hijo izquierdo 
tiene un nodo externo como su hijo izquierdo, y correspondientemente, cualquier
que no tenga un hijo derecho tiene un nodo derecho como hijo derecho
(ver la \figref{binary-tree}.b).  Es fácil de verificar, por inducción, 
que un árbol binario con $#n#\ge 1$ nodos reales tiene $#n#+1$ nodos externos.


\section{#BinaryTree#: Un Árbol Binario Básico}

\index{estructuras de datos!BinaryTree@#BinaryTree#}%
La manera más simple de representar a un nodo, #u#, en un árbol binario es 
al almacenar los vecinos (tres como máximo) de #u# explícitamente
\notpcode{:}\pcodeonly{.}
\javaimport{ods/BinaryTree.BTNode<Node}
\cppimport{ods/BinaryTree.BTNode}
Cuando uno de estos tres vecinos no está presente, le asignamos a #nil#.
De esta manera, ambos nodos externos del árbol y el padre de la raíz
corresponden al valor #nil#.

El árbol binario en si mismo puede entonces representarse con una
\javaonly{reference}\cpponly{pointer}\pcodeonly{referencia} a su nodo raíz,
#r#:
\codeimport{ods/BinaryTree.r}

Podemos calcular la profundidad de un nodo, #u#, en árbol binario al contar 
el número de pasos en el recorrido desde #u# a la raíz:
\codeimport{ods/BinaryTree.depth(u)}


\subsection{Algoritmos Recursivos}

\index{algoritmo!recursivo}%
El uso de algoritmos recursivos facilita la computación de hechos sobre 
los árboles binarios. Por ejemplo, para calcular el tamaño de (números de nodos
en) un árbol binario arraigado en el nodo #u#, recursivamente calculamos los 
tamaños de los dos subárboles arraigados en los hijos de #u#, añadimos 
sus tamaños, y agregamos uno:

\codeimport{ods/BinaryTree.size(u)}

Para calcular el tamaño de un nodo #u#, calculamos el tamaño de los dos
subárboles de #u#, tomamos el valor máximo, y agregamos uno:

\codeimport{ods/BinaryTree.height(u)}

\subsection{Recorriendo los Árboles Binarios}
\seclabel{bintree:traversal}

\index{árbol!recorrido de un}%
%\index{tree traversal}%
%\index{binary-tree traversal}%
Los dos algoritmos de las secciones previas usan recursión para visitar todos
los nodos en un árbol binario. Cada uno de ellos visita los nodos del árbol
binario en el mismo orden como en el código siguiente:
\codeimport{ods/BinaryTree.traverse(u)}

Al usar recursión de esta manera produce código simple y corto, pero puede
ser problemático. La profundidad máxima de la recursión es dada por la 
profundidad máxima de un nodo en el árbol binario, i.e., la altura del árbol.
Si la altura del árbol es muy grande, entonces esta recursión podría
usar mucho más espacio de la pila de aquel disponible, causando un 
paro del programa. 

Para recorrer un árbol binario sin recursión, puedes usar un algoritmos que
depende en el conocimiento de donde viene para determinar hacia donde irá.
Ver la \figref{bintree-traverse}.  Si llegamos a un nodo #u# desde #u.parent#,
entonces la siguiente cosa que debemos hacer es visitar a #u.left#.  Si llegamos 
a  #u# desde #u.left#, entonces la siguiente cosa que debemos hacer es 
visitar #u.right#.  Si llegamos a #u# desde #u.right#, entonces hemos terminado
con la visita al subárbol de #u#, y por lo tanto retornamos a 
#u.parent#.  El siguiente código implementa esta idea, 
con código incluido para manejar los casos donde cualquiera de #u.left#,
#u.right#, o #u.parent# es #nil#:
\codeimport{ods/BinaryTree.traverse2()}

\begin{figure}
  \begin{center}
    \begin{tabular}{cc}
      \includegraphics[scale=0.90909]{figs/bintree-traverse-2}
      \includegraphics[scale=0.90909]{figs/bintree-3}
    \end{tabular}
  \end{center}
  \caption[Recorriendo un Árbol Binario]{
    Los tres casos que ocurren en el nodo #u# cuando se recorre un 
    árbol de forma no-recursiva, y el resultante del recorrido del árbol.}
  \figlabel{bintree-traverse}
\end{figure}

Los mismos hechos que pueden calcularse con algoritmos recursivos pueden también 
calcularse de esta manera, sin recursión. Por ejemplo, para calcular el
tamaño del árbol mantenemos un contador, #n#, e incrementamos a #n# cuando 
visitamos un nodo por la primera vez:
\codeimport{ods/BinaryTree.size2()}

En algunas implementaciones de los árboles binarios, el atributo #parent# no 
se utiliza. Cuando este es el caso, una implementación no-recursiva es aún
posible, pero la implementación tiene que usar una #List# (o #Stack#) para 
mantener un récord del recorrido desde el nodo actual a la raíz.

Un tipo especial de recorrido que no encaja en el patrón de las funciones
anteriores es el \emph{recorrido de búsqueda amplia}.
%\index{breadth-first traversal}%
\index{recorrido!de búsqueda amplia}%
En un recorrido de búsqueda amplia, los nodos se visitan nivel por nivel 
comenzando en la raíz y moviéndose hacia abajo, visitando los nodos en 
cada nivel desde la izquierda hacia la derecha (
\figref{bintree-bfs}). Esto es similar a la manera en la cual leeríamos 
una página de texto en inglés. El recorrido de búsqueda amplia se implementa 
usando una cola, #q#, que inicialmente contiene solamente la raíz, #r#.
A cada paso, extraemos el siguiente nodo, #u#, desde #q#, procesamos a #u#
y agregamos a #u.left# y #u.right# (si no son #nil#) a #q#:
\codeimport{ods/BinaryTree.bfTraverse()}

\begin{figure}
  \begin{center}
    \includegraphics[scale=0.90909]{figs/bintree-4}
  \end{center}
  \caption{Durante el recorrido de búsqueda amplia, los nodos de un árbol
    binario se visitan nivel por nivel, y de izquierda a derecha dentro 
    de cada nivel.}
  \figlabel{bintree-bfs}
\end{figure}

\section{#BinarySearchTree#: Un Árbol Binario de Búsqueda Desbalanceado}
\seclabel{binarysearchtree}

\index{estructuras de datos!BinarySearchTree@#BinarySearchTree#}%
\index{árbol!binario de búsqueda}%
%\index{binary tree!search}%
Una estructura de datos #BinarySearchTree# es un tipo especial de árbol binario
en la cual cada nodo, #u#, también almacena un valor, #u.x#, desde algún 
orden tota. Los valores en árbol binario de búsqueda obedecen la
\emph{propiedad de un árbol binario de búsqueda}:
\index{propiedad!de un árbol binario de búsqueda}%
Para un nodo, #u#, cada valor almacenado en el subárbol arraigado en #u.left#
es menor que #u.x# y cada valor almacenando en el subárbol en 
#u.right# es mayor que #u.x#.  Un ejemplo de una #BinarySearchTree# 
se muestra en la \figref{bst}.

\begin{figure}
  \begin{center}
    \includegraphics[scale=0.90909]{figs/bst-example}
    %\includegraphics[scale=0.90909]{figs/binary-tree-4}
  \end{center}
  \caption{Un árbol binario de búsqueda.}
  \figlabel{bst}
\end{figure}


\subsection{Búsqueda}

\index{recorrido!de búsqueda en un árbol binario de búsqueda}%
La propiedad de un árbol binario de búsqueda es extremadamente útil porque
nos permite rápidamente localizar un valor, #x#, en un árbol binario de
búsqueda. Para hacer esto comenzamos con la búsqueda de #x# en la raíz, #r#.
Cuando un nodo, #u#, hay tres casos:
\begin{enumerate}
\item Si $#x#< #u.x#$, entonces la búsqueda procede con #u.left#;
\item Si $#x#> #u.x#$, entonces la búsqueda procede con #u.right#;
\item Si $#x#= #u.x#$, entonces hemos encontrado el nodo #u# que contiene a #x#.
\end{enumerate}
La búsqueda termina cuando el Caso~3 ocurre o cuando #u=nil#.  En el primer
caso, hemos encontrado a #x#.  En el último, concluimos que #x#
no se encuentra en el árbol binario de búsqueda.
\codeimport{ods/BinarySearchTree.findEQ(x)}

Dos ejemplos de búsquedas en un árbol binario de búsqueda se muestra en 
la \figref{bst-search}. Como muestra el segundo ejemplo, aún si no encontramos
a #x# en el árbol, todavía obtenemos información valuable. Si observamos el
último nodo, #u#, en el cual Caso~1 ocurrió, vemos que #u.x# 
es el valor más pequeño en el árbol que es mayor que #x#. Similarmente, 
el último nodo en el cual el Caso~2 ocurrió contiene el valor más grande
que es menor que #x#. Por lo tanto, al mantener un récord del último 
nodo, #z#, en el cual el Caso~1 ocurre, una estructura #BinarySearchTree#
puede implementar la operación #find(x)# que returna el valor más pequeño 
almacenado en el árbol que es mayor que o igual a #x#:
\codeimport{ods/BinarySearchTree.find(x)}

\begin{figure}
  \begin{center}
    \begin{tabular}{cc}
    \includegraphics[width=\HalfScaleIfNeeded]{figs/bst-example-2} &
    \includegraphics[width=\HalfScaleIfNeeded]{figs/bst-example-3} \\
    (a) & (b)
    \end{tabular}
  \end{center}
  \caption{Un ejemplo de (a)~una búsqueda exitosa (de $6$) y (b)~una búsqueda no
    exitosa (de $10$) en un árbol binario de búsqueda.}
  \figlabel{bst-search}
\end{figure}


\subsection{Adición}

Para agregar un valor nuevo, #x#, a una #BinarySearchTree#, primero buscamos 
a #x#. Si lo encontramos, entonces no hay necesidad de insertalo. De lo 
contrario, almacenamos a #x# en una hoja hija del último nodo, #p#, 
el cual es encontrado durante la búsqueda por #x#. Si el nodo nuevo es 
un hijo izquierdo \emph{ó} un hijo derecho de #p# depende en el resultado de la
comparación entre #x# y #p.x#.

\codeimport{ods/BinarySearchTree.add(x)}
\codeimport{ods/BinarySearchTree.findLast(x)}
\codeimport{ods/BinarySearchTree.addChild(p,u)}
Un ejemplo se muestra en la \figref{bst-insert}. La parte que consume 
más tiempo de este proceso es la búsqueda inicial por #x#, 
la cual toma un monto de tiempo proporcional a la altura del nodo buevo #u#.
En el peor de los casos, esta es igual a la alturar de la
estructura #BinarySearchTree#.


\begin{figure}
  \begin{center}
    \begin{tabular}{cc}
    \includegraphics[width=\HalfScaleIfNeeded]{figs/bst-example-4} &
    \includegraphics[width=\HalfScaleIfNeeded]{figs/bst-example-5} 
    \end{tabular}
  \end{center}
  \caption{Insertando el valor $8.5$ en un árbol binario de búsqueda.}
  \figlabel{bst-insert}
\end{figure}


\subsection{Remoción}

Eliminar un valor almacenado en un nodo, #u#, de una #BinarySearchTree# es un
poco más difícil. Si #u# es una hoja, entonces necesitamos solamente desprender
a #u# de su padre. Aún mejor: Si #u# tiene solamente un hijo, podemos
empalmar a #u# del árbol al hacer que #u.parent# adopte el hijo de #u# (ver
\figref{bst-splice}):
\codeimport{ods/BinarySearchTree.splice(u)}

\begin{figure}
  \begin{center}
    \includegraphics[scale=0.90909]{figs/bst-splice}
  \end{center}
  \caption{Removiendo una hoja ($6$) o  un nodo con un solo hijo ($9$) es fácil.}
  \figlabel{bst-splice}
\end{figure}

Aunque las cosas se complican cuando #u# tiene dos hijos.  En este caso,
lo más simple de hacer es encontrar un nod, #w#, que tenga menos de
dos hijos y tal que #w.x# pueda reemplazar a #u.x#.  Para mantener 
la propiedad de un árbol binario de búsqueda, el valor #w.x# debería ser cercano
al valor de #u.x#.  Por ejemplo, elegir a #w# de tal manera que #w.x# sea
el valor más pequeño que es mayor que #u.x# funcionaŕa. Encontrar el nodo #w#
es fácil; el valor más pequeño en el subárbol arraigado en #u.right#.  Este 
nodo puede fácilmente removerse debido a que no tiene un hijo izquierdo.
(ver \figref{bst-remove}).
\javaimport{ods/BinarySearchTree.remove(u)}
\cppimport{ods/BinarySearchTree.remove(u)}
\pcodeimport{ods/BinarySearchTree.remove_node(u)}

\begin{figure}
  \begin{center}
    \begin{tabular}{cc}
    \includegraphics[width=\HalfScaleIfNeeded]{figs/bst-delete-1}
    \includegraphics[width=\HalfScaleIfNeeded]{figs/bst-delete-2}
    \end{tabular}
  \end{center}
  \caption[Borrando en una BinarySearchTree]{Borrar un valor ($11$) de un nodo,
    #u#, con dos hijos se hace al reemplazar el valor de #u# con el valor más
    pequeño en el árbol derecho de #u#.}
  \figlabel{bst-remove}
\end{figure}

\subsection{Resumen}

Las operaciones #find(x)#, #add(x)#, y #remove(x)# en una estructura
#BinarySearchTree# involucran una ruta desde la raíz del árbol a algún nodo
en el árbol. Sin saber sobre la forma del árbol es difícil decir algo sobre la
logitud de dicha ruta,  excepto que es menos #n#, el número de nodos en el
árbol. El siguiente teorema no del todo impresionante sintetiza el rendimiento 
de una estructura de datos #BinarySearchTree#:

\begin{thm}\thmlabel{bst}
    Una #BinarySearchTree# implementa la interfaz #SSet# y soporta las
    operaciones #add(x)#, #remove(x)#,
    y #find(x)# en $O(#n#)$ tiempo por operación.
\end{thm}

\thmref{bst} se compara pésimament con \thmref{skiplist}, lo cual muestra la
estructura #SkiplistSSet# puede implementar la interfaz #SSet#
con tiempo anticipado $O(\log #n#)$ por operación.  El problema con
la estructura #BinarySearchTree#  es que se vuelve \emph{desbalanceada}.
En lugar de lucir como el árbol en la figura \figref{bst} puede lucir 
como una cadena larga de nodos #n#, los cuales tienen exactamente
un hijo excepto el último.

Existen varias formas de evitar los árboles binarios de búsqueda desbalanceados,
la cuales conllevan a estructuras de datos que tienen operaciones de tiempo
$O(\log#n#)$. En el \chapref{rbs} mostramos cómo operaciones de tiempo 
\emph{anticipado} $O(\log #n#)$ pueden lograrse con la aleatorización.
En el \chapref{scapegoat} mostramos cómo operaciones de tiempo \emph{amortizado}
$O(\log #n#)$ pueden lograrse con operaciones de reconstrucción parcial.
En el \chapref{redblack} mostramos cómo operaciones de tiempo de \emph{peor
caso}  $O(\log #n#)$ pueden lograrse al simular un árbol que no es binario.
Es decir, un árbol donde los nodos pueden tener como máximo cuatro hijos.

\section{Discusión y Ejercicios}

Los árboles binarios han sido usado para modelar relaciones por millares de
años. Una razón para esto es que los árboles binarios naturalment modelan
el árbol genealógico familiar.
\index{árbol!genealógico}%
\index{árbol!familiar}%
%\index{pedigree family tree}%
Estos son los árboles familiares en los cuales la raíz es una persona, 
los hijos izquierdo y derecho son los progenitores de la persona, y 
así recursivamente. En las siglos recientes los árboles binarios también 
han sido utilizado para modelar los árboles filogenético en biología
\index{árbol!filogenético}
\index{árbol!de especies}
en biología, donde las hojas del árbol representan las especies existentes
y los nodos internos del árbol representan los 
\emph{eventos de especiación}
\index{evento!de especiación} 
en los cuales dos poblaciones de una sola especies evolucionan en 
dos especies separadas.

Los árboles binarios de búsqueda aparentement fueron descubiertos
independientement por varios grupos en las década del 1950
\cite[Section~6.2.2]{k97v3}.  Referencias adicionales a los tipos específicos
de árboles binarios de búsqueda se proveen en los capítulos subsiguientes.

Al implementar un árbol binario desce cero, existen varias decisiones 
sobre diseño que deben hacerse. Una de estas es la pregunta de si cada
nodo debería almacenar un puntero a su padre. Si la mayoría de las operaciones
simplemente siguen una ruta de raíz-a-hoja, entonces los punteros a los padres 
son innecesarios, malgastan espacio, y son una fuente potencial de errores 
en el código. Por otro lado, la ausencia de punteros a los padres significa 
que los recorridos de un árbol deben hacerse recursivamente o con el 
uso de una pila explícita. Otros métodos (tales como la inserción o remoción 
en algunos tipos de árboles binarios de búsqueda balanceados) también 
se complican por la ausencia de punteros a los padres.

Otra decisión sobre diseño está relacionada con la forma en la cual los 
punteros al padre, al hijo izquierdo, y al hijo derecho se almacenan en 
un nodo. En la implementación dada aquí, estos punteros se almacenan en
variables separadas. Otra opción es almacenarlos en un arrat, #p#, de 
tamaño 3, de tal manera que #u.p[0]# es el hijo izquierdo de #u#, 
#u.p[1]# es el hijo derecho de #u#, y #u.p[2]# es el padre
de #u#. Al usar un array de esta manera significa que algunas secuencias 
de sentencias #if# pueden simplificarse en expresiones algrebraicas.

Una ejemplo de tal simplificación ocurre durante el recorrido de un árbol.
Si un recorrido llega a un nodo #u# desde #u.p[i]#, entonces el nodo siguiente
en el recorrido es $#u.p#[(#i#+1)\bmod 3]$.  Ejemplos similares ocurren
cuando existe un simetría de izquierda-a-derecha. Por ejemplo, el hermano 
de #u.p[i]# es $#u.p#[(#i#+1)\bmod 2]$.  Este truco funciona si #u.p[i]# es
un hijo izquierdo ($#i#=0$) o un hijo derecho ($#i#=1$) de #u#. 
En algunos casos esto significa que algún código complicado que normalmente
necesitaría ambas versiones (izquierda y derecha) puede ahora escribirse c
solamente una vez. Ver los métodos #rotateLeft(u)# y #rotateRight(u)# en la 
página~\pageref{page:rotations} para un ejemplo.

\begin{exc}
    Demuestra que un árbol binario tiene $#n#\ge 1$ nodos con $#n#-1$ aristas.
\end{exc}

\begin{exc}
    Demuestra que un árbol binario que tiene $#n#\ge 1$ nodo reales tiene $#n#+1$
    nodos externos.
\end{exc}

\begin{exc}
    Demuestra que, si un árbol binario, $T$, tiene por lo menos una hoja,
    entonces (a)~la raíz de $T$ tiene como máximo un hijo o (b)~$T$
    tiene más de una hoja.
\end{exc}

\begin{exc}
    Implementa un método no recursivo, #size2(u)#, que calcula el tamaño del
    subárbol arraigado en el nodo #u#.
\end{exc}

\begin{exc}
    Escribe un método no recursivo, #height2(u)#, que calcula la altura de un
    nodo #u# en una #BinaryTree#.
\end{exc}

\begin{exc}
    Un árbol binario es \emph{balanceado por tamaño}
    %\index{size-balanced}%
    \index{árbol!binario de búsqueda balanceado por tamaño}%
    si, por cada nodo #u#, el tamaño de los subárboles arraigados 
    #u.left# y #u.right# difieren como máximo por uno.
    Escrive un método recursivo, #isBalanced()#, que examina si un árbol 
    binario está balanceado.  Tu método debería ejecutarse en tiempo 
    $O(#n#)$.  (Asegúrate de probar tu código con árboles grandes con
    formas diferentes; es fácil escribir un métod que toma un tiempo
    mayor $O(#n#)$.)
\end{exc}

\index{recorrido!pre-order}%
\index{recorrido!post-order}%
\index{recorrido!in-order}%
%\index{pre-order traversal}%
%\index{post-order traversal}%
%\index{in-order traversal}%
Un recorrido \emph{pre-order} de una árbol binario es un recorrido 
que visita cada nodo, #u#, antes de cualquiera de sus hijos. Un recorrido
\emph{in-order} visita a #u# después de visitar todos los nodos a la izquierda
de #u# pero antes de visitar todos los nodos a la derecha de #u#.
Un recorrido \emph{post-order} visita a #u# solamente después de visitar todos
los nodos en el subárbol de #u#. 
La numeración pre/in/post-order de un árbol marca los nodos de un árbol
con los enteros $0,\ldots,#n#-1$ en el orden que son encontrados 
por un recorrido pre/in/post-order.  Ver \figref{binarytree-numbering}
por un ejemplo.

\begin{figure}
  \begin{center}
    \includegraphics[scale=0.90909]{figs/binarytree-numbering-1}
    \includegraphics[scale=0.90909]{figs/binarytree-numbering-2} \\[2ex]
    \includegraphics[scale=0.90909]{figs/binarytree-numbering-3}
  \end{center}
    \caption{Numeraciones pre-order, post-order, e in-order de un árbol binario.}
  \figlabel{binarytree-numbering}
\end{figure}

\begin{exc}
  \index{numeración!pre-order}%
  \index{numeración!post-order}%
  \index{numeración!in-order}%
  %\index{pre-order number}%
  %\index{post-order number}%
  %\index{in-order number}%
    Crear una subclase de #BinaryTree# cuyo nodos tienen atributos para
    almacenar los números pre-order, post-order, y in-order. Escribe los 
    métodos recursivos #preOrderNumber()#, #inOrderNumber()#, y
    #postOrderNumbers()# que asignan estos números correctamente. Estos métodos
    deberían ejecutarse en tiempo $O(#n#)$.
\end{exc}

\begin{exc}\exclabel{tree-traversal}
    Implementa las funciones no-recursivas #nextPreOrder(u)#, #nextInOrder(u)#,
    y #nextPostOrder(u)# que retornan el nodo que sigue a #u# en recorrido
    pre-order, in-order, o post-order, respectivamente. Estas funciones
    deberían tomar tiempo amortizado constante; si comenzamos con cualquier
    #u# y repetidamente llamamos a una de estas funciones y asignamos el
    valor retornado a #u# hasta que $#u#=#null#$, entonces el costo de todas
    llamadas debería ser $O(#n#)$.
\end{exc}

\begin{exc}
    Supón que nos dan un árbol binario con números en pre-, post-, and in-order
    asignados a los nodos. Muestra cómo estos números pueden usarse para 
    responder cada una de las siguientes preguntas en tiempo constante:
  \begin{enumerate}
    \item Dado un nodo #u#, determina el tamaño del subárbol arraigado en #u#.
    \item Dado un nodo #u#, determina determina la altura de #u#.
    \item Dado dos nodos #u# y #w#, determina si #u# es un ancestro de #w#.
  \end{enumerate}
\end{exc}

\begin{exc}
    Supón que te dan una lista de nodos con números pre-order y in-order
    asignados. Demuestra que existe como máximo un árbol possible con esta
    numeración pre-order/in-order y muestra cómo construirlo.
\end{exc}

\begin{exc}
    Demuestra que la forma de cualquier árbol binario de #n# nodos puede
    representarse usando como máximo $2(#n#-1)$ bits.  (Pista: Piensa sobre 
    la grabación de lo que sucede durante un recorrido y después reproducir
    tal grabación para reconstruir el árbol.)
\end{exc}

\begin{exc}
    Ilustra lo que pasa cuando agregramos el valor $3.5$
    y después $4.5$ al árbol binario de búsqueda en la \figref{bst}.
\end{exc}

\begin{exc}
    Ilustra lo que pasa cuando agregramos el valor $3$
    y después $5$ al árbol binario de búsqueda en la \figref{bst}.
\end{exc}

\begin{exc}
    Implementa un método de #BinarySearchTree#, #getLE(x)#,
    que retorna una lista de todos los artículos en el árbol que son
    menores o iguales a #x#. El tiempo de ejecución de tu método
    debería ser $O(#n#'+#h#)$ donde $#n#'$ es el número de artículos menores 
    que o iguales a #x# y #h# es la altura del árbol.
\end{exc}

\begin{exc}
    Describe cómo añadir los elementos $\{1,\ldots,#n#\}$ a una estructura
    #BinarySearchTree# que estaba inicialmente vacía de tal manera que el
    árbol resultante tenga la altura $#n#-1$. De cuántas maneras puede hacerse
    esto?
\end{exc}

\begin{exc}
    Si tenemos alguna estructura #BinarySearchTree# y realiza la operación #add(x)#
    seguida por #remove(x)# (con el mismo valor de #x#), ¿retornamos 
    necesariamente al árbol original?
\end{exc}

\begin{exc}
    ¿Puede una operación #remove(x)# incrementar la altura de cualquier nodo en
    una estructura #BinarySearchTree#?  Si lo hace, ¿por cuánto?
\end{exc}

\begin{exc}
    ¿Puede una operación #remove(x)# incrementar la altura de cualquier nodo en
    una estructura #BinarySearchTree#? ¿Puede incrementar la altura del árbol?
    Si lo hace, ¿por cuánto?
\end{exc}

\begin{exc}
    Diseña e implementa una versión de #BinarySearchTree# en la cual cada nodo,
    #u#, mantiene los valores #u.size# (el tamaño del subárbol arraigado en #u#),
    #u.depth# (la profundidad de #u#), y #u.height# (la altura del subárbol
    arraigado en #u#).  

    Estos valores deberían mantenerse, aún durante las llamadas a las
    operaciones #add(x)# y #remove(x)#, pero esto no debería incrementar el 
    costo de estas operaciones por más de un factor constante.
\end{exc}
