\chapter{Árboles Binarios de Búsqueda Aleatoria}
\chaplabel{rbs}

En este capítulo, presentamos una estructura árbol binario de búsqueda que
usa aleatorización para lograr un tiempo anticipado $O(\log #n#)$ para
todas las operaciones.

\section{Árboles Binarios de Búsqueda Aleatoria}
\seclabel{rbst}

Considera los árboles binarios de búsqueda mostrados en la \figref{rbs-lvc},
cada uno de los cuales tiene $#n#=15$ nodos. El de la izquierda es una lista
y el otro es un árbol binario de búsqueda perfectamente balanceado. El de la
izquierda tiene una altura de
$#n#-1=14$ y el de la derecha tiene un altura de tres.

\begin{figure}
  \begin{center}
    \begin{tabular}{cc}
      \includegraphics[scale=0.90909,scale=0.95]{figs/bst-path} &
      \includegraphics[scale=0.90909,scale=0.95]{figs/bst-balanced}
    \end{tabular}
  \end{center}
  \caption{Dos árboles binarios de búsqueda que contienen los enteros $0,\ldots,14$.}
  \figlabel{rbs-lvc}
\end{figure}

Imagina cómo estos dos árboles podrían ser construidos. Aquel en la izquierda
ocurre si comenzamos con una estructura #BinarySearchTree# vacía y agregamos
la secuencia
\[
    \langle 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14 \rangle \enspace .
\]

No otra secuencia de adiciones creará este árbol (como puedes demostrar
por inducción con #n#). Por otro lado, el árbol en la derecha puede crearse
con la secuencia
\[
    \langle 7,3,11,1,5,9,13,0,2,4,6,8,10,12,14 \rangle  \enspace .
\]

Otras secuencias funciona también, incluyendo
\[
    \langle 7,3,1,5,0,2,4,6,11,9,13,8,10,12,14 \rangle  \enspace ,
\]
y
\[
    \langle 7,3,1,11,5,0,2,4,6,9,13,8,10,12,14 \rangle \enspace .
\]

De hecho, existen $21,964,800$ secuencias de adición que genera el árbol
en la derecha y solamente uno que genera el árbol en la izquierda.

El ejemplo más arriba nos da evidencia anecdótica que, si elegimos una
permutación aleatoria de $0,\ldots,14$, y la agregamos a un árbol binario de
búsqueda, entonces es más probable que obtengamos un árbol muy balanceado
(el lado derecho \figref{rbs-lvc}) que un árbol muy desbalanceado (
el lado izquierdo  de \figref{rbs-lvc}).

Podemos formalizar esta noción al estudiar árboles binarios de
búsqueda aleatoria.

Un \emph{árbol binario de búsqueda aleatoria}
\index{árbol!binario de búqueda aleatoria}%
%\index{binary search tree!random}%
de tamaño #n# se obtiene en la siguiente manera:
Toma una permutación aleatoria, $#x#_0,\ldots,#x#_{#n#-1}$,
de los enteros $0,\ldots,#n#-1$ y agrega sus elementos, uno a uno,
a una estructura #BinarySearchTree#. Por \emph{permutación aleatoria}
\index{permutación!aleatoria}%
%\index{random permutation}%
queremos decir que cada una de las posibles $#n#!$ permutaciones (ordenamientos) de
$0,\ldots,#n#-1$ es igualmente probable, así que la probabilidad de obtener
cualquier permutación en particular es $1/#n#!$.

Nota que los valores $0,\ldots,#n#-1$ podrían ser reemplazados por cualquier
ordenamiento de un conjunto de #n# elementos sin cambiar cualquier ninguna de
las propiedades del árbol binario de búsqueda aleatoria. El elemento
$#x#\in\{0,\ldots,#n#-1\}$ simplemente representa el elemento de rango
#x# en un conjunto ordenado de tamaño #n#.

Antes que presentemos nuestro resultado principal sobre los árboles binarios
de búsqueda aleatoria, debemos tomar algo de tiempo para discutir brevemente
un tipo de número que aparece frecuentemente al estudiar estructuras aleatorias.
Para un número entero no-negativo, $k$, el \emph{número harmónico} $k$-th,
\index{número!harmónico}%
\index{H@$H_k$ (número harmónico)}%
denotado $H_k$, es definido como

\[
  H_k = 1 + 1/2 + 1/3 + \cdots + 1/k \enspace .
\]

El número harmónico $H_k$ no posee un forma simple cerrada, pero está
íntimamente relacionado al logaritmo natural de $k$.  En particular,

\[
  \ln k < H_k \le \ln k + 1  \enspace .
\]
\newcommand{\hint}{\int_1^k\! (1/x)\, \mathrm{d}x}%

Los lectores que han estudiado cálculo podrán notar que esto es debido a que
la integral $\hint = \ln k$. Considerando que una integral pude interpretarse
como el área entre una curva y el eje-$x$, el valor de $H_k$ puede tener un
límite inferior con la  integral $\hint$ y un límite superior con
$1+ \hint$.  (Ver la \figref{harmonic-integral} para una explicación gráfica.)

\begin{figure}
  \begin{center}
    \begin{tabular}{cc}
      \includegraphics[width=\HalfScaleIfNeeded]{figs/harmonic-2}
        & \includegraphics[width=\HalfScaleIfNeeded]{figs/harmonic-3}
    \end{tabular}
  \end{center}
  \caption{El número harmónico $k$th $H_k=\sum_{i=1}^k 1/i$ tiene un límite
    superior e inferior de dos integrales. El valor de estas integrales es dado
    por el área de la región sombreada, mientras que el valor de
    $H_k$ es dado por el área de los rectángulos.}
  \figlabel{harmonic-integral}
\end{figure}


\begin{lem}\lemlabel{rbs}
    En un árbol binario de búsqueda aleatoria de tamaño #n#, las siguientes
    sentencias se mantienen:
  \begin{enumerate}
    \item Para cualquier $#x#\in\{0,\ldots,#n#-1\}$, la longitud anticipada de
        la ruta de búsqueda de #x# es $H_{#x#+1} + H_{#n#-#x#} -
          O(1)$.\footnote{Las expresiones $#x#+1$ y $#n#-#x#$ pueden
          interpretarse respectivamente como el número de elementos en el árbol
          menor que o igual a #x# y el número de elementos en el árbol mayor que
          o igual a #x#.}
    \item Para cualquier $#x#\in(-1,n)\setminus\{0,\ldots,#n#-1\}$, la longitud
        anticipada de la ruta de búsqueda para #x# es $H_{\lceil#x#\rceil}
    + H_{#n#-\lceil#x#\rceil}$.
  \end{enumerate}
\end{lem}

En la siguiente sesión demostraremos \lemref{rbs}. Por ahora, considera lo que
las dos partes de \lemref{rbs} nos dicen. La primera parte nos dice que si
buscamos un elemento en un árbol de tamaño #n#, entonces la longitud anticipada
de la ruta de búsqueda es como máximo $2\ln n + O(1)$. La segunda parte nos
dice lo mismo para la búsqueda de un valor que no está almacenado en el árbol.
Cuando comparamos las dos partes del lema, vemos que es solamente un
poquito más rápido buscar por algo que se encuentra en el árbol comparado
con aquello que no se encuentra en el mismo.

\subsection{Demostración del \lemref{rbs}}

La observación principal necesaria para demostrar el \lemref{rbs} es la
siguiente: La ruta de búsqueda para un valor #x# en el intervalo abierto
$(-1,#n#)$ en un árbol binario de búsqueda aleatoria, $T$, contiene el nodo
con la llave $i < #x#$ si y solo si, en la permutación aleatoria usada
para crear a $T$, $i$ aparece antes que cualquier $\{i+1,i+2,\ldots,\lfloor#x#\rfloor\}$.

Para ver esto, refiérete a \figref{rbst-records} y nota que hasta que algún
valor en $\{i,i+1,\ldots,\lfloor#x#\rfloor\}$ es añadido, las rutas de búsqueda
para cada valor en el intervalo abierto $(i-1,\lfloor#x#\rfloor+1)$ son
idénticas. (Recuerda que para que dos valores tengan rutas de búsqueda
diferentes, un elemento debe existir en el árbol de tal manera que compare
diferentemente con ellos.)  Sea $j$ el primer elemento en
$\{i,i+1,\ldots,\lfloor#x#\rfloor\}$ que aparece en la permutación aleatoria.
Nota que $j$ está ahora y siempre estará en la ruta de búsqueda para #x#.
Si $j\neq i$ entonces el nodo $#u#_j$ que contiene a $j$ es creado antes que el
nodo $#u#_i$ que contiene a $i$.  Después, cuando $i$ es añadido, el mismo será
añadido al subárbol arraigado en $#u#_j#.left#$, dado que $i<j$.  Por el otro
lado, la ruta de búsqueda para #x# nunca visitará este subárbol porque
procederá a $#u#_j#.right#$ antes de visitar a $#u#_j$.

\begin{figure}
  \begin{center}
    \includegraphics[width=\ScaleIfNeeded]{figs/rbst-records}
  \end{center}
    \caption[La ruta de búsqueda en un árbol binario de búsqueda aleatoria]
    {El valor $i<#x#$ se encuentra en ruta de búsqueda para #x# si y solo si
    $i$ es el primer elemento entre $\{i,i+1,\ldots,\lfloor#x#\rfloor\}$ añadido
    al árbol.}
  \figlabel{rbst-records}
\end{figure}

Similarmente, para $i>#x#$, $i$ aparece el ruta de búsqueda para #x#
si y solo si $i$ aparece antes que cualquiera de los valores $\{\lceil#x#\rceil,
\lceil#x#\rceil+1,\ldots,i-1\}$ en la permutación aleatoria usada para crear
a $T$.

Nota que, si comenzamos con una permutación aleatoria de $\{0,\ldots,#n#\}$,
entonces las subsecuencias que contiene solamente $\{i,i+1,\ldots,\lfloor#x#\rfloor\}$
y $\{\lceil#x#\rceil, \lceil#x#\rceil+1,\ldots,i-1\}$ son también permutaciones
aleatorias de sus elementos respectivos. Cada elemento, entonces, en los
subconjuntos $\{i,i+1,\ldots,\lfloor#x#\rfloor\}$ and $\{\lceil#x#\rceil,
\lceil#x#\rceil+1,\ldots,i-1\}$ es igualmente probable de aparecer antes
que cualquier otro en su subconjunto en la permutación aleatoria usada para
crear a $T$.
Así que tenemos
\[
  \Pr\{\mbox{$i$ se encuentra en la ruta de búsqueda para #x#}\}
  = \left\{ \begin{array}{ll}
     1/(\lfloor#x#\rfloor-i+1) & \mbox{if $i < #x#$} \\
     1/(i-\lceil#x#\rceil+1) & \mbox{if $i > #x#$}
     \end{array}\right . \enspace .
\]

Con esta observación, la demostración del \lemref{rbs} involucra algunas
calculaciones simples con los números harmónicos:

\begin{proof}[Demostración del \lemref{rbs}]
Sea $I_i$ la variable indicadora aleatoria que es igual a uno cuando
$i$ aparece en ruta de búsqueda para #x# y cero por lo contrario. Entonces
la longitud de la ruta de búsqueda es dada por
\[
  \sum_{i\in\{0,\ldots,#n#-1\}\setminus\{#x#\}} I_i
\]
so, if $#x#\in\{0,\ldots,#n#-1\}$, la longitud anticipada de la ruta de búsqueda
es dada por (ver la \figref{rbst-probs}.a)
\begin{align*}
  \E\left[\sum_{i=0}^{#x#-1} I_i + \sum_{i=#x#+1}^{#n#-1} I_i\right]
   & =  \sum_{i=0}^{#x#-1} \E\left[I_i\right]
         + \sum_{i=#x#+1}^{#n#-1} \E\left[I_i\right] \\
   & = \sum_{i=0}^{#x#-1} 1/(\lfloor#x#\rfloor-i+1)
         + \sum_{i=#x#+1}^{#n#-1} 1/(i-\lceil#x#\rceil+1) \\
   & = \sum_{i=0}^{#x#-1} 1/(#x#-i+1)
         + \sum_{i=#x#+1}^{#n#-1} 1/(i-#x#+1) \\
   & = \frac{1}{2}+\frac{1}{3}+\cdots+\frac{1}{#x#+1} \\
   & \quad {} + \frac{1}{2}+\frac{1}{3}+\cdots+\frac{1}{#n#-#x#} \\
   & = H_{#x#+1} + H_{#n#-#x#} - 2  \enspace .
\end{align*}
Las calculaciones correspondientes para un valor de búsqueda
$#x#\in(-1,n)\setminus\{0,\ldots,#n#-1\}$ son casi idénticas (ver la
\figref{rbst-probs}.b).
\end{proof}

\begin{figure}
  \begin{center}
    \begin{tabular}{@{}c@{}}
      \includegraphics[width=\ScaleIfNeeded]{figs/rbst-probs-a} \\ (a) \\[2ex]
      \includegraphics[width=\ScaleIfNeeded]{figs/rbst-probs-b} \\ (b) \\[2ex]
    \end{tabular}
  \end{center}
  \caption[Las probabilidades de un elemento de encontrarse en una ruta de
    búsqueda]{Las probabilidades de un elemento de encontrarse en una ruta de
    búsqueda para #x# cuando (a)~#x# es un entero y (b)~cuando #x# no es un
    entero.}
  \figlabel{rbst-probs}
\end{figure}

\subsection{Resumen}

El teorema siguiente sintetiza el rendimiento de un árbol binario de búsqueda
aleatoria:

\begin{thm}\thmlabel{rbs}
Un árbol binario de búsqueda aleatoria puede construirse en tiempo
$O(#n#\log #n#)$. En un árbol binario de búsqueda aleatoria, la operación
#find(x)# toma tiempo anticipado $O(\log#n#)$.
\end{thm}

Deberíamos enfatizar nuevamente que la expectación en
\thmref{rbs} es con respecto a la permutación aleatoria usada para crear
el árbol binario de búsqueda aleatoria. En particular, no depende en la elección
aleatoria de #x#; es verdadero para cualquier valor de #x#.

\section{#Treap#: Un Árbol Binario de Búsqueda Aleatoria}
\seclabel{treap}
\index{estructuras de datos!Treap@#Treap#}%

El problema de los árboles binarios de búsqueda aleatoria es que, por supuesto,
los mismos no son dinámicos. Ellos no soportan las operaciones
#add(x)# o #remove(x)#, la cuales son necesarias para implementar la interfaz
#SSet#. En esta sección, describiremos una estructura de datos conocida como
una estructura #Treap# que usa el lema \lemref{rbs} para implementar la interfaz #SSet#.
\footnote{El nombre #Treap# proviene del hecho que esta estructura de datos
es simultáneamente un árbol binario de búsqueda
(binary search \textbf{tr}ee [\secref{binarysearchtree}]) y un montículo (
h\textbf{eap} [\chapref{heaps}]).}

Un nodo en una estructura #Treap# es similar a un nodo en una #BinarySearchTree#
en que es un valor de dato, #x#, pero también contiene una \emph{prioridad}
numérica única, #p#, que es asignada aleatoriamente:
\javaimport{ods/Treap.Node<T>}
\cppimport{ods/Treap.TreapNode}
Además de ser un árboles binarios de búsqueda, los nodos en una estructura
#Treap# también obedecen la \emph{propiedad de un montículo}:
\begin{itemize}
\item (Propiedad de un Montículo)  En cada nodo #u#, excepto la raíz,
      $#u.parent.p# < #u.p#$.
\index{propiedad!de un montículo}%
\end{itemize}
En otras palabras, cada nodo tiene una propiedad más pequeña que aquellas
propiedades de sus hijos. Un ejemplo se muestra en la \figref{treap}.

\begin{figure}
  \begin{center}
    \includegraphics[width=\ScaleIfNeeded]{figs/treap}
  \end{center}
  \caption[Una Treap]{Un ejemplo de una estructura #Treap# que contiene los
    enteros $0,\ldots,9$. Cada nodo, #u#, se ilustra como una caja que contiene
    $#u.x#,#u.p#$.}
  \figlabel{treap}
\end{figure}

Las condiciones del montículo y el árbol binario de búsqueda aseguran que, una
vez la llave (#x#) y la prioridad (#p#) para cada nodo están definidas,
la forma de la estructura #Treap# está completamente determinada.
La propiedad de un montículo nos dice que el nodo con una prioridad miníma
tiene que ser la raíz, #r#, de #Treap#. La propiedad del árbol binario de
búsqueda nos dice que todos los nodos con llaves más pequeñas que
#r.x# están almacenados en el subárbol arraigado en #r.left# y todos los nodos
con llaves mayores que #r.x# están almacenados en el árbol arraigado en
#r.right#.

El punto importante acerca de los valores de prioridad en una estructura
#Treap# es que los mismos son únicos y asignados aleatoriamente. Debido a esto,
existen dos maneras equivalentes de pensar sobre una #Treap#.  Como se define más
arriba, una #Treap# obedece las propiedades del montículo y del árbol binario de
búsqueda. Alternativamente, podemos pensar sobre una #Treap# como un estructura
#BinarySearchTree# cuyos nodos fueron añadidos en orden creciente de prioridad.
Por ejemplo, la estructura #Treap# en la \figref{treap} puede obtenerse al
añadir la secuencia de valores $(#x#,#p#)$
\[
  \langle
   (3,1), (1,6), (0,9), (5,11), (4,14), (9,17), (7,22), (6,42), (8,49), (2,99)
  \rangle
\]
a una #BinarySearchTree#.

Dado que las prioridades fueron eligidas aleatoriamente, esto es equivalente a
tomar una permutación aleatoria de las llaves---en este caso la permutación es
\[
  \langle 3, 1, 0, 5, 9, 4, 7, 6, 8, 2 \rangle
\]
---y añadirlas a una estructura #BinarySearchTree#.  Pero esto significa que la forma de
un \emph{treap} es idéntica a la forma de un árbol binario de búsqueda
aleatoria. En particular, si reemplazamos cada llave #x# por su rango,
\footnote{El rango de un elemento #x# en un conjunto $S$ de elementos es el
número de elementos en $S$ que son menor que #x#.}, entonces el
\lemref{rbs} applica. Reiterando el \lemref{rbs} en términos de las estructuras
#Treap#, tenemos:
\begin{lem}\lemlabel{rbs-treap}
    En una estructura #Treap# que almacena un conjunto $S$ de #n# llaves, las
    sentencias siguientes se mantienen:
  \begin{enumerate}
    \item Para cualquier $#x#\in S$, la longitud anticipada de la ruta de
        búsqueda para #x# es $H_{r(#x#)+1} + H_{#n#-r(#x#)} - O(1)$.
    \item Para cualquier $#x#\not\in S$, la longitud anticipada de la ruta de
        búsqueda para #x# es $H_{r(#x#)} + H_{#n#-r(#x#)}$.
  \end{enumerate}
  Aquí, $r(#x#)$ denota el rango de #x# en el conjunto $S\cup\{#x#\}$.
\end{lem}
nuevamente, enfatizamos que la expectación en el \lemref{rbs-treap} se toma
sobre las elecciones aleatorias de las prioridadades para cada nodo. No
se requiere ninguna suposición sobre la aleatoriedad en las llaves.

\lemref{rbs-treap} nos dice que las estructuras #Treap#s pueden implementar la
operación #find(x)# eficientement. Sin embargo, el beneficio real de una
estructura #Treap#
es que puede soportar las operaciones #add(x)# y #delete(x)#.
Para hacer esto, dicha estructura necesita realizar rotaciones para mantener la
propiedad del montículo. Consulta a \figref{rotations}.

Un \emph{rotación}
\index{rotación}%
en un árbol binario de búsqueda es una modificación local que toma un padre
#u# de un nodo #w# y convierte a #w# en el padre de #u#, mientras preserva la
propiedad del árbol binario de búsqueda. Las rotaciones aparecen en dos
sabores: \emph{izquierda} o \emph{derecha} dependiendo si
#w# es un hijo derecho o un hijo izquierdo de #u#, respectivamente.
\index{rotación!izquierda}%
\index{rotación!derecha}%

\begin{figure}
  \begin{center}
     \includegraphics[width=\ScaleIfNeeded]{figs/rotation}
  \end{center}
  \caption{Las rotaciones izquierda y derecha en un árbol binario de búsqueda.}
  \figlabel{rotations}
\end{figure}

El código que implementa esto tiene que manejar dos posibilidades y
ser cuidadoso de un caso límite (cuando #u# es la raíz), así que el código
actual es un poquito más largo que lo que la \figref{rotations} conduciría al
lector a creer:
\codeimport{ods/BinarySearchTree.rotateLeft(u).rotateRight(u)}
\label{page:rotations}
En términos de la estructura de datos #Treap#, la priorida más importante de una
rotación es que la profundidad de #w# disminuye por uno mientras la profundidad
de #u# aumenta por uno.

Usando rotaciones, podemos implementar la operacióm #add(x)# de la siguiente
manera: Creamos un nodo, #u#, asignamos #u.x=x#, y elegimos un valor aleatorio
para #u.p#. Consecuentemente añádimos #u# usando el algoritmo usual #add(x)#
para una estructura #BinarySearchTree#, de tal manera que #u# es ahora una
hoja de la estructura #Treap#. A esta altura, nuestra estructura #Treap#
satifisface la propiedad de un árbol binario de búsqueda,
pero no necesariamente la propiedad de un montículo.En particular, podría ser
el caso que #u.parent.p > u.p#.  Si este es el caso, entonces realizamos una
rotación en el nodo #w#=#u.parent# de tal manera que #u# se convierte en el
padre de #w#. Si #u# continúa violando la propiedad de un montículo, tendremos
que repetir esto, disminuyendo la profundidad de #u# por uno en cada ocasión,
hasta que #u# se convierte en la raíz \emph{ó} $#u.parent.p# < #u.p#$.
\codeimport{ods/Treap.add(x).bubbleUp(u)}
Un ejemplo de una operación #add(x)# se muestra en la \figref{treap-add}.

\begin{figure}
  \begin{center}
  \includegraphics[width=\ScaleIfNeeded]{figs/treap-insert-a} \\
  \includegraphics[width=\ScaleIfNeeded]{figs/treap-insert-b} \\
  \includegraphics[width=\ScaleIfNeeded]{figs/treap-insert-c} \\
  \end{center}
  \caption[Añadiendo a un treap]{Añadiendo el valor 1.5 a la
    estructura #Treap# de la \figref{treap}.}
  \figlabel{treap-add}
\end{figure}

El tiempo de ejecución anticipado de la operación #add(x)# es dado por el tiempo
que toma en seguir la ruta de búsqued para #x# más el número de rotaciones
realizadas para mover el nuevo nodo, #u#, hasta su posición correcta en la
estructura #Treap#.  Por el \lemref{rbs-treap}, la longitud anticipada de la
ruta de búsquea es como máxima $2\ln #n#+O(1)$.  Además, cada rotación
disminuye la profundidad de #u#. Esto finaliza si #u# se convierte en la raíz,
así que el número anticipado de rotaciones no puede exceder la longitud
anticipada de la ruta de búsqueda. Por lo tanto, el tiempo de ejecución
anticipado de la operación #add(x)# en una estructura #Treap# es
$O(\log #n#)$.  (\excref{treap-rotates} requiere que muestre que el número
anticipado de rotaciones durante una adición es actualmente solamente
$O(1)$.)

La operación #remove(x)# en una estructura #Treap# es opuesta a la operación
#add(x)#. Buscamos por un nodo, #u#, que contiene a #x#, después hacemos
las rotaciones para mover #u# hacia abajo hasta que se convierta en un hoja,
y después separamos a #u# desde la estructura #Treap#.  Nota que, para mover a
#u# hacia abajo, podemos realizar una rotación izquierda o derecha en #u#, la
cual reemplazará a #u# con #u.right# o #u.left#, respectivamente.

La decisión se hace con la primera opción de las siguientes que aplica:
\begin{enumerate}
    \item Si #u.left# y #u.right# son ambos #null#, entonces #u# es una hoja y no
        se realiza ninguna rotación.
    \item Si #u.left# (o #u.right#) es #null#, entonces un rotación derecha (o
        izquierda, respectivamente) en #u#.
    \item Si $#u.left.p# < #u.right.p#$ (o $#u.left.p# > #u.right.p#)$, entonces una
    rotación derecha (rotación izquierda, respectivamente) en #u#.
\end{enumerate}

Estas tres reglas aseguran que la estructura #Treap# no sea desconectada y que la
propiedad de montículo sea restaurada una vez que #u# es removido.
\codeimport{ods/Treap.remove(x).trickleDown(u)}

Un ejemplo de la operación #remove(x)# se muestra en la  \figref{treap-remove}.
\begin{figure}
  \begin{center}
  \includegraphics[height=\QuarterHeightScaleIfNeeded]{figs/treap-delete-a} \\
  \includegraphics[height=\QuarterHeightScaleIfNeeded]{figs/treap-delete-b} \\
  \includegraphics[height=\QuarterHeightScaleIfNeeded]{figs/treap-delete-c} \\
  \includegraphics[height=\QuarterHeightScaleIfNeeded]{figs/treap-delete-d}
  \end{center}
  \caption[Removiendo de un treap]{Removiendo el valor 9 de la estructura
    #Treap# en la \figref{treap}.}
  \figlabel{treap-remove}
\end{figure}

El truco para analizar el tiempo de ejecución de la operación #remove(x)# es
notar que esta operación invierte la operación #add(x)#. En particular,
si fuéramos a reinsertar a #x#, usando la misma prioridad #u.p#, entonces la
operación #add(x)# haría exactamente el mismo número de rotaciones y restoraría
la estructura #Treap# a exactamente el mismo estado en el cual se encontraba
antes que la operación #remove(x)# fuese aplicada. (Leyendo desde abajo hacia
arriba, la \figref{treap-remove} ilustra la adición del valor 9 a una
estructura #Treap#.) Esto significa que el tiempo de ejecución anticipado de
la operación #remove(x)# sobre una estructura #Treap# de tamaño #n# es
proporcional al tiempo de ejecución anticipado de la operación #add(x)# sobre
una estructura #Treap# de tamaño $#n#-1$. Concluimos que el tiempo de ejecución
anticipado de #remove(x)# es $O(\log #n#)$.

\subsection{Resumen}

El teorema siguiente sintetiza el rendimiento de la estructura de datos
#Treap#:

\begin{thm}
Una #Treap# implementa la interfaz #SSet#. Una #Treap# soporta las operaciones
#add(x)#, #remove(x)#, y #find(x)# en tiempo anticipado $O(\log #n#)$ por
operación.
\end{thm}

Merece la pena comparar  la estructura de datos #Treap# a la estructura de datos
#SkiplistSSet#. Ambas implementan la operaciones #SSet# en tiempo anticipado
$O(\log #n#)$ por operación. En ambas estructuras, #add(x)# y #remove(x)#
involucran una búsqued y subsecuentemente un número constante de cambios de
punteros (ver \excref{treap-rotates} más abajo).  Así que, para ambas estructuras,
la longitud anticipada de la ruta de búsqueda es el valor critical al evaluar
el rendimiento de las mismas. En una #SkiplistSSet#, la longitud anticipada de
un ruta de búsqueda
\[
     2\log #n# + O(1) \enspace ,
\]
En una #Treap#, la longitud anticipada de una ruta de búsqueda es
\[
    2\ln #n# +O(1) \approx 1.386\log #n#  + O(1) \enspace .
\]
Así que, las rutas de búsqueda en una #Treap# son considerablemente mucho más
cortas y esto se traduce en operaciones muchos más rápidas en #Treap#s que
en #Skiplist#s.
\excref{skiplist-opt} en \chapref{skiplists} muestra cómo la longitud anticipada
de la ruta de búsqueda en una #Skiplist# puede reducirse a
\[
     e\ln #n# + O(1) \approx 1.884\log #n# + O(1)
\]
al usar los lanzamientos de monedas parciales. Aún con esta optmización, la
longitud anticipada de las rutas de búsqueda en una #SkiplistSSet# es
notablemente más larga que en una #Treap#.

\section{Discusión y Ejercicios}

Los árboles binarios de búsqueda aleatoria se han estudiado extensivamente.
Devroye \cite{d88} provee una demostración del \lemref{rbs} y resultados
relacionados. Existen resultados mucho más firmes en la literatura, de los
cuales el más impresionante es debido Reed \cite{r03}, quien muestra que la
altura anticipada de un árbol binario de búsqueda aleatoria es
\[
  \alpha\ln n - \beta\ln\ln n + O(1)
\]
donde $\alpha\approx4.31107$ es la solución única en el intervalo
$[2,\infty)$ de la ecuación $\alpha\ln((2e/\alpha))=1$ y
$\beta=\frac{3}{2\ln(\alpha/2)}$. Además, la varianza de la altura es
constante.

El nombre #Treap# fue acuñado por Seidel y Aragon \cite{as96} quienes
discutieron las estructuras #Treap#s y algunas de sus variaciones.
No obstante, su estructura básica fue estudiada por Vuillemin \cite{v80} quien
las llamó árboles Cartesianos.

Una posible optimización espacial de la estructura de datos #Treap# es la
eliminación del almacenamiento explícito de la prioridad #p# en cada nodo.
En lugar, la prioridad de un nodo, #u#, se calcular al hashear la
dirección de #u# en la memoria\javaonly{ (en Java 32-bit, esto es equivalente
a hashear #u.hashCode()#)}.  Aunque un número de funciones hash probablemente
funcionarán bien en práctica, para que las partes importantes de la demostración
de \lemref{rbs} permanezcan válidas, la función hash debería ser aleatorizada
y tener la \emph{propiedad independiente relativo al mínimo}:

\index{independiente!relativo al mínimo}%
\index{propiedad!independiente relativo al mínimo}%
Para cualesquiera valores distintos $x_1,\ldots,x_k$, cada uno de los valores
hash $h(x_1),\ldots,h(x_k)$ debería ser distinto con alta probabilidad y, por
cada $i\in\{1,\ldots,k\}$,
\[
   \Pr\{h(x_i) = \min\{h(x_1),\ldots,h(x_k)\}\} \le c/k
\]
para alguna constante $c$.

Una clase de funciones hash que es fácil de implementar y relativamente rápida
es \emph{tabulation hashing} (\secref{tabulation}).
%\index{tabulation hashing}%
\index{hashing!tabulación}%

Otra variante de una estructura #Treap# que no requiere el almacenamiento de
prioridades en cada nodo es el árbol binario de búsqueda aleatorizada
\index{árbol!binario de búsqueda aleatorizado}%
%\index{binary search tree!randomized}%
de Mart\'\i nez y Roura \cite{mr98}. En esta variante, cada nodo, #u#, almacena
el tamaño, #u.size#. del subárbol arraigado en #u#. Ambos algoritmos
#add(x)# and #remove(x)# son aleatorizados. El algoritmo para añadir a #x# al
subárbol arraigado en #u# hace lo siguiente:

\begin{enumerate}
   \item Con probabilidad $1/(#size(u)#+1)$, el valor #x# es añadido de la
    manera usual, como una hoja, y después se hacen algunas para traer a #x#
    a la raíz de este subárbol.
   \item De lo contrario (con probabilidad $1-1/(#size(u)#+1)$), el valor
    #x# se añade recursivamente a uno de los subárboles arraigados en
    #u.left# o #u.right#, como sea apropiado.
\end{enumerate}

El primer caso corresponde a una operación #add(x)# en una #Treap# donde
el nodo de #x# recibe una prioridad aleatoria que es más pequeña que cualquiera
de las #size(u)# prioridades en el subárbol de #u#, y este caso ocurre
con exactamente la misma probabilidad.

Remover un valor #x# de un árbol binario de búsqueda aleatorizado es similar
al proceso de removerlo desde una #Treap#.  Encontramos el nodo, #u#,
que contiene a #x# y después realizamos algunas rotaciones que repetidamente
aumentan la profundidad de #u# hasta que se convierta en un hoja, a cual punto
podemos separarlo del árbol. La decisión de si realizamos una rotación izquierda
o derecha en cada paso es aleatorizada.

\begin{enumerate}
  \item Con probabilidad #u.left.size/(u.size-1)#, realizamos una rotación
    derecha en #u#, convirtiendo a #u.left# en la raíz del subárbol que estaba
    anteriormente arraigado en #u#.
  \item  Con probabilidad #u.right.size/(u.size-1)#, realizamos una rotación
    izquierda #u#, convirtiendo a #u.right# en la raíz del subárbol que estaba
    anteriormente arraigado en #u#.
\end{enumerate}

Nuevamente, podemos verificar que estas son exactamente las mismas
probabilidades que el algoritmo de remoción en una estructura #Treap# realizará
una rotación izquierda o derecha de #u#.

Los árboles binarios de búsqueda aleatorizados tienen la desventaja, comparados
con los treaps, que cuando los elementos son añadidos y removidos ellos realizan
decisiones aleatorizadas, y deben mantener los tamaños de los subárboles. Una
ventaja de los árboles binarios de búsqueda aleatorizados sobre los treaps
es que los tamaños de los subárboles puede servir otro propósito útil, el cual
es proveer acceso por rango en tiempo anticipado $O(\log #n#)$
(ver \excref{treap-get}).  En comparación, las prioridades aleatorias
almacenadas en los nodos de un treap no tienen otro uso aparte de mantener el
treap balanceado.

\begin{exc}
    Ilustra la adición de 4.5 (con prioridad 7) y después 7.5 (con prioridad 20)
    a la estructura #Treap# en la \figref{treap}.
\end{exc}

\begin{exc}
    Ilustra la remoción de 5 y después de 7 en la estructura #Treap# en la
    \figref{treap}.
\end{exc}

\begin{exc}
    Demuestra la aserción que hay $21,964,800$ secuencias que generan el árbol
    en el lado derecho de la \figref{rbs-lvc}.  (Pista: Provee una fórmula
    recursiva para el número de secuencias que genera una árbol binario completo
    de altura $h$ y evalúa esta fórmula $h=3$.)
\end{exc}

\begin{exc}
    Diseña e implementa el método #permute(a)# que toma como entrada un array,
    #a#, que contienen #n# valores distintos y permuta a #a# aleatoriamente.
    El método debe ejecutarse en tiempo $O(#n#)$ y debería demostrar que cada
    de las $#n#!$ permutaciones posibles de #a# es igualmente probable.
\end{exc}

\begin{exc}\exclabel{treap-rotates}
    Usa ambas partes del \lemref{rbs-treap} para demostrar que el número
    anticipado de rotaciones realizado por una operación #add(x)# (y de igual
    manera una operación #remove(x)#) es $O(1)$.
\end{exc}

\begin{exc}
    Modifica la implementación de la estructura #Treap# dada aquí de tal manera
    que no almacene prioridades explícitamente. En lugar de eso, debería
    simularlas al aplicar una función hash al #hashCode()# de cada nodo.
\end{exc}

\begin{exc}
    Supón que un árbol binario de búsqueda almacena, en cada nodo, #u#, la
    altura, #u.height#, del subárbol arraigado en #u#, y el tamaño, #u.size#,
    del subárbol arraigado en #u#.
  \begin{enumerate}
    \item Muestra cómo, si realizamos una rotación izquierda o derecha en #u#,
        entonces estas dos cantidades pueden actualizarse, en tiempo constante,
        para todos los nodos afectados por la rotación.
    \item Explica por qué el mismo resultado no es posible si también
        almacenamos la profundidad, #u.depth#, de cada nodo #u#.
  \end{enumerate}
\end{exc}

\begin{exc}
    Diseña e implementa un algoritmo que construye una estructura #Treap# usando
    un array ordenado, #a#, de #n# elementos. Este método debería ejecutarse en
    tiempo de peor caso $O(#n#)$ y debería construir una estructura #Treap#
    que es indistinguible de otra estructura #Treap# en la cual los elementos
    de #a# fueron añadidos uno a la vez usando el método #add(x)#.
\end{exc}

\begin{exc}
  \index{dedo}%
  \index{búsqueda!de dedo en un treap}%
    Este ejercicio detalla cómo se puede buscar en una estructura #Treap#
    dado un puntero que es cercano al nodo que estamos buscando.
  \begin{enumerate}
    \item Diseña e implementa una implementación de #Treap# en la cual cada nodo
        mantiene un récord de los valores mínimos y máximos en su subárbol.
    \item Usando esta información extra, agrega un método #fingerFind(x,u)#
        que ejecuta la operación #find(x)# con la ayuda de un puntero al nodo
        #u# (el cual no se encuentra lejo del nodo que contiene a #x#).
        Esta operación debería comenzar con #u# y caminar hacia arriba
        hasta que alcance un nodo #w# tal que $#w.min#\le #x#\le #w.max#$.
        Desde este punto en adelante, debería realizar una búsqueda estándar
        por #x# comenzando desde #w#. (Uno puede mostrar que #fingerFind(x,u)#
        toma tiempo $O(1+\log r)$, donde $r$ es el número de elementos en el
        treap cuyo valor se encuentra entre #x# and #u.x#.)
    \item Extiende tu implementación en una versión de un treap que comienza
        todas sus operaciones #find(x)# desde el nodo que fue encontrado más
        recientemente por #find(x)#.
  \end{enumerate}
\end{exc}

\begin{exc}\exclabel{treap-get}
    Diseña e implementa una versión de una estructura #Treap# que incluya una
    operación  #get(i)# que retorna la llave con rango #i# en #Treap#.  (Pista:
    Haz que cada nodo, #u#, mantenga un récord del tamaño del subárbol
    arraigad en #u#.)
\end{exc}

\begin{exc}
  \index{estructuras de datos!TreapList@#TreapList#}%
    Implementa una estructura de datos #TreapList#, una implementación de la
    interfaz #List# como un treap. Cada nodo del treap debería almacenar una
    lista de artículos, y un recorrido \textit{in-order} del treap encuentra los
    artículos en el mismo orden en el cual ocurren en la lista. Todas la
    operaciones #get(i)#, #set(i,x)#, #add(i,x)# y #remove(i)# de #List#
    deberían ejecutarse en un tiempo anticipado $O(\log #n#)$.
\end{exc}

\begin{exc}\exclabel{treap-split}
    Diseña e implementa una versión de una estructura #Treap# que soporta la
    operación #split(x)#. Esta operación remueve todos los valores desde
    la estructura #Treap# que son mayores que #x# y retorna una segunda
    estructura #Treap# que contiene todos los valores removidos.

  \noindent Ejemplo: el código #t2 = t.split(x)# remueve desde #t# todos los
    valores mayores que #x# y retorna una estructura nueva #Treap# #t2# que
    contiene todos estos valores. La operación #split(x)# debería ejecutarse en
    tiempo anticipado $O(\log #n#)$.

  \noindent Advertencia: Para que esta modificación funcione apropiadamente y
    aún permita que el método #size()# se ejecute en tiempo constante, es
    necesario implementar las modificaciones en \excref{treap-get}.
\end{exc}

\begin{exc}\exclabel{treap-join}
    Diseña e implementa una versión de una estructura #Treap# que soporta la
    operación #absorb(t2)#, la cual puede considerarse como la versión inversa
    de la operación #split(x)#.  Esta operación remueve todos los valores desde
    #Treap# #t2# y los agrega al recibidor. Esta operación presupone que el
    valor más pequeño en #t2# es mayor que el valor más grande en el recibidor.
    La operación #absorb(t2)# debería ejecutarse en tiempo anticipado
    $O(\log #n#)$.
\end{exc}

\begin{exc}
    Implementa los árboles binarios de búsqueda aleatoria de Martínez,
    como se discutió en esta sección. Compara el rendimiento de tu
    implementación con aquella implementación de #Treap#.
\end{exc}

