dir = chapters
sources = $(dir)/intro.tex $(dir)/arrays.tex 
          #$(dir)/linkedlists.tex \
		  #$(dir)/skiplists.tex\
		  #$(dir)/hashing.tex\
		  #$(dir)/binarytrees.tex\
		  #$(dir)/rbs.tex\
		  #$(dir)/scapegoat.tex\
		  #$(dir)/redblack.tex\
		  #$(dir)/heaps.tex\
		  #$(dir)/sorting.tex\
		  #$(dir)/graphs.tex\
		  #$(dir)/integers.tex\
		  #$(dir)/btree.tex
python = $(wildcard ../python/ods/*.py)
pyts = $(sources:.tex=-python.tex)
pyth = $(sources:.tex=-python-html.tex)
pdfs = ods-python.pdf
styles=ods.sty ods-colors.sty

LFLAGS=-interaction=nonstopmode -halt-on-error

#all: $(pdfs)
all: index-inclusion

.PHONY: all figs images install clean
#.PHONY: all install clean

figs:
	cd figs ; make
	cd figs-python ; make
   

epsfigs:
	cd figs ; make epss
	cd figs-python ; make epss

images:
	cd images ; make

index-inclusion: ods-python.pdf makeindex ods-python.idx ods-python.pdf

ods-python.pdf : ods-python.tex $(pyts) $(styles) images figs
	latexmk -pdf ods-python

ods-python.tex : ods.tex Makefile
	echo "%%%\n%%% Autogenerated file - do not edit \n%%%" | cat - ods.tex \
		| sed 's/-lang/-python/' > ods-python.tex
	sed -i 's/%HEADCOMMAND/\\newcommand{\\javaonly}[1]{}\\newcommand{\\cpponly}[1]{}\\newcommand{\\pcodeonly}[1]{#1}\\newcommand{\\notpcode}[1]{}\\newcommand{\\lang}{pseudocode}/' ods-python.tex


%-python.tex : %.tex $(python) snarf-python.py Makefile
	echo "%%%\n%%% Autogenerated file - do not edit \n%%%" | cat - $< \
		| ./snarf-python.py > $*-python.tex 

install-pdf : ods-python.pdf ods-python-screen.pdf
	scp -r $^ morin@cg.scs.carleton.ca:public_html/ods/

install-html : python-html
	scp -r  ods-python/ morin@cg.scs.carleton.ca:public_html/ods/

install : install-pdf install-html

clean:
	rm -f $(pyts) $(pyth) *.log *.bbl *.out ods.toc $(pdfs)
	rm -f ods-python.{aux,fls,fdb_latexmk,idx,ilg,ind,text,toc,pdf}
	rm -f $(dir)/*.aux
	cd figs; make clean
	cd figs-python; make clean
	cd images; make clean

# A dirty hack for getting a grayscale file
%-grayscale.pdf : %.pdf
	gs -sOutputFile=$@ -sDEVICE=pdfwrite \
	-sColorConversionStrategy=Gray -dProcessColorModel=/DeviceGray \
	-dCompatibilityLevel=1.4 $< < /dev/null

# Lots of hackery for generating html using latex2html
html: python-html

python-html : $(pyth) ods-python-html.tex epsfigs
	mkdir -p ods-python
	pdflatex ods-python-html
	bibtex ods-python-html
	latex2html -init_file latex2html-init -dir ods-python ods-python-html

ods-python-html.tex : ods-python.tex Makefile
	sed 's/-python/-python-html/' ods-python.tex > ods-python-html.tex

%-python-html.tex : %.tex $(pyt) snarf-cpp-html.pl Makefile
	cat $< | ./snarf-python.py -html > $*-python-html.tex 

%-screen.pdf : %.pdf
	gs -o $@ -sDEVICE=pdfwrite -c "[/CropBox [53 88 379 595] /PAGES pdfmark" -f $<

